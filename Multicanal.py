#!/usr/bin/env python
# Imports para uso geral
import os
import sys
import bm3d
import numpy as np
import scipy.fft as fft
import cv2
from matplotlib import pyplot as plt
from matplotlib import cm
from scipy import signal, linalg
from copy import deepcopy

# ---------------------------------------
#  Módulo de funções de pré-processamento
# ---------------------------------------

def img_norm (mat):
    '''Função de normalização de imagem após processamento.'''

    # Determinação de máximos e mínimos
    mmax = np.max(mat)
    mmin = np.min(mat)

    # Cálculo de matriz normalizada
    ret = ((mat - mmin) / (mmax - mmin)) * 255
    return ret.astype(np.uint8)

def img_norm_float (mat):
    '''Função de normalização de imagem após processamento.'''

    # Determinação de máximos e mínimos
    mmax = np.max(mat)
    mmin = np.min(mat)

    # Cálculo de matriz normalizada
    ret = ((mat - mmin) / (mmax - mmin))
    return ret

# Função de cálculo de matriz de convolução 2D
def convmatx2 (ker, size):
    '''Função de determinação de matriz de convolução
       para sinais bidimensionais usada para decomposição
       via matriz de Toeplitz. Referencia usada:
       https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.convolution_matrix.html

       Relação de inputs:
        1 - ker: Kernel a ser "vetorizado" para a matriz de conv.;
        2 - size: Tupla com tamanho da matriz de sa'ida. Percebe-se que 
            a relaç~ao de tamanhos 'e dada por: 
                  size[0] = s_ker[0] + s_x[0] - 1
                  size[1] = s_ker[1] + s_x[1] - 1'''

    vec_ker = ker.ravel()
    ret = linalg.convolution_matrix(vec_ker, size)
    return ret

# Função de estimação de PSNR
def calc_psnr (ref_img, lat_img):
    '''Função de estimação de PSNR de uma determinada imagem latente

       Relação de inputs:
        1 - ref_img: Imagem de referência
        2 - lat_img: Imagem latente para estimação da grandeza'''

    # Confere se as dimensões das imagens são adequadas e as normalizam
    if ref_img.shape != lat_img.shape:
        raise Exception('Imagens de dimensões diferentes, averiguar ocorrido.')

    MSE = np.mean((ref_img - lat_img)**2)
    MAX = np.max(ref_img)
    if MSE == 0:
        return 100

    PSNR = 20 * np.log10(MAX / np.sqrt(MSE))
    return PSNR

# Função de amostragem de floats diferentes entre si
def sample_floats (lo, hi, k=1):
    '''Função de escolha de números de ponto flutuante
       diferentes. Essa função será utilizada para a 
       determinação do desvio padrão dos canais gaussianos.

       Relação de inputs:
        1 - lo: Limite inferior
        2 - hi: Limite superior
        3 - k: Número de amostras'''

    ret  = np.zeros(k)
    seen = set()
    for ii in range(k):
        x = np.random.uniform(lo, hi)
        while x in seen:
            x = np.random.uniform(lo, hi)

        seen.add(x)
        ret[ii] = x

    return ret

# Função de determinação de kernel gaussiano
def gen_gaussian_kernel (size, sigma, param=3):
    '''Função de criação de kerneis gaussianos de determinado
       tamanho e variância

       Relação de inputs:
        1 - size: Dimensão de kernel (quadrado - ímpar)
        2 - sigma: Desvio padrão do kernel'''

    # Verifica se o tamanho em questão é ímpar
    if size % 2 == 0:
        size += 1

    # Criação de fase aleatória para o sistema
    if param != 0:
        ph_x, ph_y = np.random.choice(
            np.arange(-param, param), size=2
        )
    else:
        ph_x, ph_y = 0, 0

    # Definição offsets e índices geradores
    x0 = (size / 2) - 1 + ph_x
    y0 = (size / 2) - 1 + ph_y
    x  = np.arange(size)

    # Cálculo de gaussiana unidimensional e matriz
    gx  = np.exp(-(x - x0)**2 / (2 * sigma)**2)
    gy  = np.exp(-(x - y0)**2 / (2 * sigma)**2)
    G   = np.outer(gx, gy)
    G  /= np.sum(G.sum())
    return G[:(size - 1), :(size - 1)]

# Função para zeropad de imagens
def zero_pad (img, ker_size):
    '''Função de normalização de matrizes com diferentes tamanhos
       para evitar convolução circular e efeitos de espelhamento
       nas bordas.
       Relação de inputs:
        1 - img: Matriz contendo a imagem originial;
        2 - fft_sz: Próximo tamanho de janelamento eficiente.'''

    # Estimação de comprimento adequado
    im_x, im_y = img.shape
    s_fx       = fft.next_fast_len(ker_size[0] + im_x - 1)
    s_fy       = fft.next_fast_len(ker_size[1] + im_y - 1)

    # Criação da matriz de zeros e encaixe da imagem
    Mat               = np.zeros((s_fx, s_fy))
    Mat[:im_x, :im_y] = im
    return Mat

# Função de acréscimo de ruído branco 
def awgn (img, snr=20):
    '''Função de inserção de ruído AWGN em imagem de
       referência a partir de um determinado nível de
       relação sinal ruído (em dB).
       Relação de inputs:
        1 - img: Imagem de referência
        2 - snr: Valor da relação sinal ruído [dB]'''

    # Determinação da potência total da imagem
    pot_img = np.sum(img ** 2) / (img.shape[0] * img.shape[1])

    # Determinação da potência de ruído [dB]
    snr_cst   = np.power(10, snr / 10)
    pot_noise = pot_img / snr_cst

    # Criação do ruído branco e soma com a imagem de interesse
    noise = np.sqrt(pot_noise / 2) * np.random.standard_normal(
        img.shape
    )
    n_img = img + noise
    return n_img

#---------------------------------------------------
# Módulo de borramento de imagem e criação de canais
#---------------------------------------------------

def setup (p_flag=True, simp_flag=True, noise_flag=True, SNR=20):
    '''Função de inicialização de figuras borradas e estruturas com kernels
       de borramento podendo conter ou não ruído'''

    # Inicializa seed para RNG
    np.random.seed()

    # Leitura da imgem original
    im_s = (64, 64)
    im   = cv2.imread('starfish.jpeg', cv2.IMREAD_GRAYSCALE)
    im   = cv2.resize(im, im_s)
    s_fx = fft.next_fast_len(im.shape[0])
    s_fy = fft.next_fast_len(im.shape[1])
    #im   = cv2.resize(im, (s_fx, s_fy))

    # Criação dos n canais gaussianos de forma iterativa
    radiusPSF = 10
    size_k    = 4 #2 * radiusPSF + 1
    n         = 2 #5
    lo, hi    = (1, size_k / 2) #radiusPSF)

    # zero padding imagem e borrão
    s_x       = 2 * s_fx + 2 * size_k + 1 #s_fx + 2 * radiusPSF + 1
    s_y       = 2 * s_fx + 2 * size_k + 1 #s_fy + 2 * radiusPSF + 1
    ker       = np.zeros((s_x, s_y, n))
    sig_vec   = sample_floats(lo, hi, k=n)
    blured_im = np.zeros((s_x, s_y, n))

    # Aplicação dos diferentes kerneis na imagem de referência
    X_tmp = fft.fft2(im, (s_x, s_y))
    im    = fft.ifft2(X_tmp).real
    #for ii, sig in enumerate(sig_vec):
    #    k_tmp               = np.random.rand(size_k, size_k) #gen_gaussian_kernel(size_k, sig, size_k)
    #    k_tmp              /= k_tmp.sum().sum()              # TESTE    
    #    K_tmp               = fft.fft2(k_tmp, (s_x, s_y))    #fft.fft2(ker[:, :, ii], (s_x, s_y))
    #    ker[:, :, ii]       = fft.ifft2(K_tmp).real
    #    BIMG_tmp            = K_tmp * X_tmp
    #    #blured_im[:, :, ii] = fft.ifft2(BIMG_tmp).real
    #
    #    # acrescenta ruído
    #    tmp                 = fft.ifft2(BIMG_tmp).real
    #    blured_im[:, :, ii] = awgn(tmp, snr=30)

    if simp_flag is True:
        ker[:size_k, :size_k, 0]  = np.array([
            [0., 0., 0., 0.],
            [0., 1., 1., 0.],
            [0., 1., 1., 0.],
            [0., 0., 0., 0.]
        ])
        ker[:size_k, :size_k, 1]  = np.array([
            [1., 1., 1., 1.],
            [1., 0., 0., 1.],
            [1., 0., 0., 1.],
            [1., 1., 1., 1.]
        ])
    else:
        ker[:size_k, :size_k, 0] = np.array([
            [0.28137002, 0.3885972 , 0.75671795, 0.20035163],
            [0.91013721, 0.59792427, 0.9617708 , 0.90449515],
            [0.08687498, 0.83484564, 0.03296456, 0.00838778],
           [0.31106622, 0.31390819, 0.81356953, 0.63244295]
        ])
        ker[:size_k, :size_k, 1]  = np.array([
            [0.00432619, 0.56073908, 0.67914993, 0.14950353],
            [0.59215767, 0.11979369, 0.49662964, 0.05516508],
            [0.24583893, 0.87801624, 0.73582039, 0.03805045],
            [0.48026675, 0.63361043, 0.60828755, 0.62653676]
        ])

    ker[:, :, 0] /= ker[:, :, 0].sum().sum()
    ker[:, :, 1] /= ker[:, :, 1].sum().sum()

    # Borramento de imagem a partir de produto das TF's
    K_tmp1              = fft.fft2(ker[:, :, 0], (s_x, s_y))
    BIMG_tmp1           = K_tmp1 * X_tmp
    tmp1                = fft.ifft2(BIMG_tmp1).real
    K_tmp2              = fft.fft2(ker[:, :, 1], (s_x, s_y))
    BIMG_tmp2           = K_tmp2 * X_tmp
    tmp2                = fft.ifft2(BIMG_tmp2).real

    # Verifica se a flag de inserção de ruído está habilitada
    if noise_flag is True:
        blured_im[:, :, 0] = awgn(tmp1, snr=SNR)
        blured_im[:, :, 1] = awgn(tmp2, snr=SNR)
    else:
        blured_im[:, :, 0] = tmp1
        blured_im[:, :, 1] = tmp2

    #---------------------------------------------------------
    # Impressão das imagens obtidas em comparação com original
    #---------------------------------------------------------

    if p_flag is True:
        plt.imshow(im[:s_fx, :s_fy], cmap='gray')
        #plt.title('imagem original')
        plt.title('Original Image')
        plt.axis('off')
        plt.show()
        print('---------------------------')

        for ii in range(n):
            # Impressão dos resultados preliminares iniciais
            plt.subplot(1, 2, 1)
            plt.imshow(blured_im[:s_fx, :s_fy, ii], cmap='gray')
            #plt.title('Imagem borrada')
            plt.title('Blurred Image')
            plt.axis('off')
            plt.subplot(1, 2, 2)
            plt.imshow(ker[:size_k, :size_k, ii], cmap='gray')
            plt.title(f'Kernel {ii+1}')
            plt.axis('off')
            plt.show()
            print(im.shape)
            print(blured_im.shape)
            print(ker[:,:,0].shape)
            print("---------------------------")

    # Cria argumento de retorno para fç de setup
    ret           = dict()
    ret['img']    = im
    ret['ker']    = ker
    ret['b_img']  = blured_im
    ret['num_ch'] = n
    ret['s_fx']   = s_fx
    ret['s_fy']   = s_fy
    ret['sx']     = im_s[0]
    ret['sy']     = im_s[1]

    # Retorna argumento de saída
    return ret

#-----------------------------------------------
# Funções de calculo de fç de custo e jacobiano
#-----------------------------------------------

def sec_au (func, Y, K_vec, X, d, lbda=0.1, iter_max=1000):
    '''Função de busca inexata de tamanho de passo por meio do
       método da seção áurea, nele computa-se um tamanho de passo
       satisfatório para a descida de gradiente a partir da assunção
       da monotonicidade da função, visto a convexidade do problema.
       Relação de inputs:
        1 - func: Objeto funcional para avaliação da fç de custo;
        2 - x0: Ponto inicial da busca;
        3 - d: Direação da realização da busca (-grad).'''

    # Declaração de parâmetros importantes para a execução do algoritmo
    theta_1 = (3 - np.sqrt(5)) / 2
    theta_2 = 1 - theta_1
    eps     = 1e-10
    rho     = 1
    b_max   = 1e8
    D       = fft.fft2(d)

    # Determinação do intervalo de interesse para observação da fç: [a, b]
    a     = 0
    s     = rho
    b     = 2 * rho
    phi_b = func(Y, K_vec + b * D,  X=X, mu=lbda)
    phi_s = func(Y, K_vec + s * D,  X=X, mu=lbda)
    ii    = 0

    while phi_b < phi_s and 2 * b < b_max and ii < iter_max:
        a     = s
        s     = b
        b     = 2*b
        phi_s = phi_b
        phi_b = func(Y, K_vec + b * D, X, lbda)
        #ii   += 1

    # Determinação do comprimento do passo em si a paritir de [a, b]
    u     = a + theta_1 * (b - a)
    v     = a + theta_2 * (b - a)
    phi_u = func(Y, K_vec + u * D,  X=X, mu=lbda)
    phi_v = func(Y, K_vec + v * D,  X=X, mu=lbda)
    ii    = 0

    while b - a > eps and ii < iter_max:
        #ii += 1

        if phi_u < phi_v:
            b     = v
            v     = u
            u     = a + theta_1 * (b - a)
            phi_v = phi_u
            phi_u = func(Y, K_vec + u * D, X=X, mu=lbda)
        else:
            a     = u
            u     = v
            v     = a + theta_2 * (b - a)
            phi_u = phi_v
            phi_v = func(Y, K_vec + v * D, X=X, mu=lbda)

    # Cálculo do passo como ponto médio do intervalo [u, v]
    return (u + v) / 2

def global_search (func, Y, K_vec, X, d, lbda=0.1, num=5):
    '''Função de avaliação de melhor passo dentro do 
       intervalo (0, 1) quantizado pelo número num.
       Relação de inputs:
        1 - func: Objeto funcional para avaliação da fç de custo;
        2 - x0: Ponto inicial da busca;
        3 - d: Direação da realização da busca (-grad).'''

    # Determinação do intervalo
    grid = np.linspace(0.05, 1.0, num)

    # Normalização da direção
    _, _, n = Y.shape
    n_jac   = 0
    for ii in range(n):
        n_jac += linalg.norm(d[:, :, ii]) ** 2

    # Descobre a fft da direção
    if n_jac > 1e3:
        D      = fft.fft2(d / np.sqrt(n_jac))
    else:
        D      = fft.fft2(d)

    # Adquire valores em cada passo da fç
    vals = [func(Y, K_vec + vv * D,  X=X, mu=lbda) / n_jac for vv in grid]

    # Adquire valor em que o mínimo ocorre
    idx  = np.argmin(vals)

    # Retorna valores mínimos encontrados
    return grid[idx]

def global_search_x (func, denoiser, Y, K_vec, X, d,
                     mu=0.1, lbda=0.1, num=5):
    '''Função de avaliação de melhor passo dentro do 
       intervalo (0, 1) quantizado pelo número num.
       Funç~ao pensada para otimizaç~ao do passo 'x'
       Relação de inputs:
        1 - func: Objeto funcional para avaliação da fç de custo;
        2 - x0: Ponto inicial da busca;
        3 - d: Direação da realização da busca (-grad).'''

    # Determinação do intervalo
    grid  = np.linspace(0.0, 1e-10, num) #num * 1e-15, num) # Aproxima passo da tol.

    # Normalização da direção
    D     = fft.fft2(d)
    x     = reg_vals(fft.ifft2(X).real)
    d     = reg_vals(d)

    # Aloca estrutura de saida
    vals = np.zeros_like(grid)

    # Loop de processamento da informaçao
    for ii, vv in enumerate(grid):
        noi      = np.dot(
            (x + vv * d).ravel(),
            (x + vv * d - denoiser((x + vv * d).astype(np.uint8))).ravel()
        ) #linalg.norm((x + vv * d).T @ (x + vv * d - denoiser((x + vv * d).astype(np.uint8))))
        vals[ii] = func(Y, K_vec,  X=X + vv * D, mu=mu) + lbda * noi

    # Adquire valor em que o mínimo ocorre
    idx  = np.argmin(vals)

    # Retorna valores mínimos encontrados
    return grid[idx]

def cost_fun (Y, K_vec, X=None, mu=0.1):
    '''Função de custo utilizada para a descida de gradiente.
       ela recebe reproduz o lagrangeano a partir da equaçao
       disposta no modelo
       Relação de inputs:
        1 - Y: Estrutura contendo as imagens captadas;
        2 - K_vec: Estrutura contendo os Kerneis estimados;
        3 - X: Imagem de referência para minimização do erro no dom. da freq.;
        4 - mu: Parâmetro de regularização associado ao erro de reconst.'''

    # Declaração das variáveis utilizadas
    _, _, n = Y.shape
    lag     = 0
    tmp     = np.zeros_like(Y[:, :, 0])

    # Declara flag de verificação de existência de X
    flag_x = False
    if not isinstance(X, type(None)):
        flag_x = True

    # Iteração por cada componente
    for ii in range(n):
        KK = K_vec[:, :, ii]
        YY = Y[:, :, ii]

        # Computação da fç de custo
        if flag_x is True:
            tmp += reg_vals(X * KK - YY)
            lag += mu * linalg.norm(fft.ifft2(tmp).real) ** 2

        # Laço para computação dos termos de multicanal
        for jj in range(ii + 1, n):
            c_tmp = fft.ifft2(
                Y[:, :, jj] * KK - YY * K_vec[:, :, jj]
            ).real
            lag  += linalg.norm(reg_vals(c_tmp)) ** 2

    return lag

def cost_fun_jac (Y, K_vec, X=None, mu=0.1):
    '''Funçao de custo utilizada para a descida de gradiente.
       ela recebe reproduz o lagrangeano a partir da equaçao
       disposta no modelo
       Relação de inputs: 
        1 - Y: Estrutura contendo as imagens captadas;
        2 - K_vec: Estrutura contendo os Kerneis estimados;
        3 - X: Imagem de referência para minimização do erro no dom. da freq.;
        4 - mu: Parâmetro de regularização associado ao erro de reconst.'''

    # Declaração das variáveis utilizadas
    sx, sy, n = Y.shape

    # Constroi cada partícula por canal
    err = np.zeros_like(K_vec[:, :, 0])
    jac = np.zeros(K_vec.shape)

    # Declara flag de verificação de existência de X
    flag_x = False
    if not isinstance(X, type(None)):
        flag_x = True

    # Cálculo do erro entre kerneis
    for ii in range(n):
        KK  = K_vec[:, :, ii]
        YY  = Y[:, :, ii]

        # Verifica se há termo de erro de reconstrução
        if flag_x is True:
            # Calcula termo do erro de reconstrução
            err            = reg_vals(X * KK - YY)
            jac[:, :, ii] += mu * fft.ifft2(
                np.conj(X) * err
            ).real

        # Calcula erro e componentes do jacobiano
        for jj in range(ii + 1, n):
            tmp = (Y[:, :, jj] * KK) - (YY * K_vec[:, :, jj])
            jac[:, :, ii] += fft.ifft2(
                np.conj(Y[:, :, jj]) * tmp
            ).real
            jac[:, :, jj] += fft.ifft2(
                np.conj(-YY) * tmp
            ).real

    return 2 * jac

#-------------------------------------
# Descida de gradiente pelo método BB
#-------------------------------------

def is_inv (mat):
    '''Função resposável pela verificação se a matriz "mat" é ou não
       inversível.
       Rlação de inputs:
        1 - mat: Matriz avaliada'''

    # Avaliação das condições de de inversibilidade
    mat  = reg_vals(mat)
    cond = np.linalg.cond(mat)
    ret  = True if cond < 1 / sys.float_info.epsilon else False
    return ret

def bb_des_k (Y, K_vec, X=None, sk=4, lbda=1e-3, mu_0=0.5,
              iter_max=1000, p_flag=False, eps=1e-5):
    '''Implementação do método quase-Newton de Barzilai-Borwein
       que aproxima a matrix hessiana para direção de descida por meio
       da aproximação da direção de Newton pela diferença sucessiva
       dos vetores gradiente em cada iteração do método.
       Relação de inputs:
        1 - Y: Estrutura contendo as imagens captadas;
        2 - K_vec: Estrutura contendo os Kerneis estimados;
        3 - X: Imagem de referência para minimização do erro no dom. da freq.;
        4 - idx: Índice do kernel sendo otimizado;
        5 - sigma: Parâmetro de atenuação dos gradientes;
        6 - mu: Parâmetro de suavização da convergência;
        7 - iter_max: Parâmetro de número máximo de iterações da descida;
        8 - p_flag: Parâmetro de indicação de impressão (p/ debug);
        9 - eps: Parâmetro de tolerância de gradiente.'''

    # Declaração de variáveis
    k_0         = fft.ifft2(K_vec).real
    K_ret       = K_vec
    jac_0       = np.zeros(k_0.shape)
    jac_1       = np.zeros(k_0.shape)
    ii          = 0
    s_x, s_y, n = Y.shape

    # Iteração inicial da descida de gradiente
    jac_0     = cost_fun_jac(Y, K_vec, X=X, mu=lbda)
    jac_0     = reg_vals(jac_0)
    jac_v     = np.zeros(iter_max)
    lag_v     = np.zeros(iter_max)
    err_vec   = np.zeros((n, iter_max))
    jac_v[0]  = linalg.norm(jac_0.ravel())
    n_jac     = linalg.norm(jac_0)
    if n_jac > 1e3:
        jac_0 /= n_jac

    # Atualiza o kernel com a variação do grad
    k_1                      = k_0 - mu_0 * jac_0
    k_1[sk:, sk:, :] = 0.0
    k_1[:sk, sk:, :] = 0.0
    k_1[sk:, :sk, :] = 0.0
    K_ret                    = fft.fft2(k_1)

    # Inicialização de variáveis de estatística de debug
    a1_c = 0
    a2_c = 0
    mu_c = 0

    # Início da iteração do algoritmo principal
    while ii < iter_max and n_jac > eps:
        # Computa gradiente da K-ésima iteração
        print(f'\r{ii + 1}/{iter_max}', end='')
        jac_1 = cost_fun_jac(
            Y,
            K_ret,
            X=X,
            mu=lbda
        )

        # Regulariza a matriz jacobiana
        for jj in range(n):
            n_jac      = linalg.norm(reg_vals(jac_1[:, :, jj]))
            jac_v[ii] += n_jac ** 2
        
        jac_v[ii] = np.sqrt(jac_v[ii])
        lag_v[ii] = cost_fun(Y, fft.fft2(k_1), X=X, mu=lbda)

        # Computa diferenças entre passo passado e gradiente passado
        ss    = k_1.ravel()   - k_0.ravel()
        yy    = jac_1.ravel() - jac_0.ravel()
        tmp1  = np.dot(yy, yy)
        tmp2  = np.dot(ss, yy)

        # Avaliação das condições de inversibilidade de tmp1 e tmp2
        if tmp1 >= 1e-8:
            # Calcula o passo a_1
            mu    = tmp2 / tmp1
            a1_c += 1

        elif tmp2 >= 1e-8:
            # Calcula passo a_2 em caso de excessão
            mu    = np.dot(ss, ss) / tmp2
            a2_c += 1

        else:
            # Em caso de ambos os passos n estarem definidos, steepest descent
            mu    = mu_0
            mu_c += 1

        # Atualiza as variáveis e aplica o passo calculado
        for jj in range(n):
            k_0[:, :, jj]             = fft.ifft2(K_ret[:, :, jj]).real
            k_1[:, :, jj]             = k_0[:, :, jj] - mu * jac_1[:, :, jj]
            k_1[sk:, sk:, jj]         = 0.
            k_1[:sk, sk:, jj]         = 0.
            k_1[sk:, :sk, jj]         = 0.
            jac_0[:, :, jj]           = jac_1[:, :, jj]
            K_ret[:, :, jj]           = fft.fft2(k_1[:, :, jj])

            # Atualiza vetor de erros.
            err_vec[jj, ii] = linalg.norm(
                (k_1[:sk, :sk, jj] / np.sum(k_1[:sk, :sk, jj])) - 
                (ker[:sk, :sk, jj] / np.sum(ker[:sk, :sk, jj]))
            )

        # Incrementa contador de iterações
        ii += 1

    # Impressão de estatísticas de debug
    if p_flag is True:
        print()
        print('ESTATÍSTICAS PARA DEBUG:')
        print(f'pct a_1: {np.round(100 * a1_c / (ii + 1))}%')
        print(f'pct a_2: {np.round(100 * a2_c / (ii + 1))}%')
        print(f'pct mu:  {np.round(100 * mu_c / (ii + 1))}%')
        print(f'passo:   {mu}')
        t = np.arange(ii)
        plt.subplot(2, 1, 1)
        plt.plot(t, jac_v[:ii])
        plt.ylabel(r'$||\nabla \mathcal{L}||$')
        plt.subplot(2, 1, 2)
        plt.plot(t, lag_v[:ii])
        plt.ylabel(r'$\mathcal{L}$')
        plt.show()

    return K_ret, err_vec, lag_v

#----------------------------
# Algoritmo de máxima descida
#----------------------------

def grad_des_k (Y, K_vec, X=None, sk=4, lbda=1e-3,
                mu=1e-3, iter_max=1000, p_flag=False, tol=0.0):
    '''Função de descida de gradiente baseada na descida de
       gradiente implementada pelo Kenji, utiliza as funções
       de custo RED e seu jacobiano para tanto.
       Relação de inputs:
        1 - Y: Estrutura contendo as imagens captadas;
        2 - K_vec: Estrutura contendo os Kerneis estimados;
        3 - X: Imagem de referência para minimização do erro no dom. da freq.;
        4 - idx: Índice do kernel sendo otimizado;
        5 - sigma: Parâmetro de atenuação dos gradientes;
        6 - mu: Parâmetro de suavização da convergência;
        7 - iter_max: Parâmetro de número máximo de iterações da descida;
        8 - p_flag: Parâmetro de indicação de impressão (p/ debug);
        9 - tol: Parâmetro de tolerância de gradiente.'''

    # Estimação das DFT's das imagens utilizadas
    ii          = 0
    n_jac       = 1                        # Inicializa a norma do jacobiano
    jac_v       = np.zeros((iter_max))     # Vetor para debug
    lag_v       = np.zeros((iter_max))     # Vetor para debug
    s_x, s_y, n = Y.shape                  # Estimação do número de canais
    err_vec     = np.zeros((n, iter_max))
    K_ret       = deepcopy(K_vec)

    # Estimação do vetor a ser computado
    jac         = np.zeros(K_vec.shape)
    mu          = 0.5
    sec_au_flag = False

    # Iterações da descida de gradiente
    while ii < iter_max and n_jac > tol:
        print(f'\r{ii + 1}/{iter_max}', end='')

        # Calculo da matriz jacobiana da função de custo
        jac       = cost_fun_jac(Y, K_ret, X=X, mu=lbda)
        lag       = cost_fun(Y, K_ret, X=X, mu=lbda)
        lag_v[ii] = lag

        # Início de computação da descida em si
        n_jac = 0
        for jj in range(n):
            n_jac += linalg.norm(jac[:, :, jj]) ** 2

        # Normalização do gradiente
        if sec_au_flag is False:
            if n_jac > 1e-3:
                jac /= np.sqrt(n_jac)

        jac_v[ii] = np.sqrt(n_jac)

        # Definição do tamanho do passo por meio do método da seção áurea
        if sec_au_flag is True:
            #mu    = sec_au(cost_fun, Y, K_ret, X, -1 * jac, lbda=lbda)
            mu    = global_search(cost_fun, Y, K_ret, X, -1 * jac, lbda=lbda)

        # Zera todos os termos fora do tamanho do filtro
        for jj in range(n):
            k                      = fft.ifft2(K_ret[:, :, jj]).real

            # Regularizaç~ao da norma l1/l2 -- TESTE
            # NORMA L1
            # tmp                    = np.zeros_like(k)
            # tmp[np.nonzero(k > 0)] = 1
            # tmp[np.nonzero(k < 0)] = -1
            # NORMA L2
            tmp                    = 2 * k          # Derivada de k.T k --> POSSIBILIDADE:
                                                    #                   usar como derivada
                                                    #                   a express~ao
                                                    # 2 * fft.ifft2(np.conj(K_ret[:, :, kk])).real
            jac[:, :, jj]         += 1e-5 * tmp 

            k                      = k - mu * jac[:, :, jj]
            k[sk:, sk:]            = 0.
            k[:sk, sk:]            = 0.
            k[sk:, :sk]            = 0.
            #k                     /= k.sum().sum()
            K_ret[:, :, jj]        = fft.fft2(k)

            # Atualiza vetor de erros.
            #err_vec[jj, ii] = linalg.norm(
            #    (k[:sk, :sk]       / np.sum(k[:sk, :sk])) - 
            #    (ker[:sk, :sk, jj] / np.sum(ker[:sk, :sk, jj]))
            #)

        # Incrementa contador de iterações
        ii += 1

    # Ciclo de debug
    if p_flag is True:
        # Plotagem dos kernels
        for jj in range(n):
            k = fft.ifft2(K_ret[:, :, jj]).real
            plt.subplot(1, 2, jj+1)
            plt.imshow(k[:sk, :sk], cmap='gray')
            plt.title(f'Estimativa de K_{jj+1}')
            plt.axis('off')

        plt.show()

        # Plotagem da fç de custo
        plt.semilogy(lag_v[1:ii])
        plt.title(r'$\mathcal{L}$')
        plt.ylabel('Função de custo')
        plt.xlabel('Iteração')
        plt.show()

        # Plotagem da fç de custo
        plt.semilogy(jac_v[1:ii])
        plt.title(r'$||\nabla \mathcal{L}||_2$')
        plt.ylabel('Gradiente da função de custo')
        plt.xlabel('Iteração')
        plt.show()

    return K_ret, err_vec, lag_v

def fixed_point_x (X, K_vec, Y, shape, lbda=1e-3,
                   mu=1e-3, iter_max=1000, p_flag=False):
    '''Função de minimização da imagem latente x_hat a partir dos novos
       kerneis estimados. Para esta modelagem ustiliza-se do algoritmo 
       RED para sua implementação em ponto fixo, dessa forma, minimiza-se
               L(X, K, Y) + lbda * x^T(x - f(x)) -> f(): denoiser
       Relação de inputs:
        1 - x: Imagem latente inicial;
        2 - k: Tensor de kerneis (domínio dos pixels);
        3 - Y: Tensor de DFTs das imagens distorcidas
        4 - size_im: Tupla com dimensões da imagem no domínio dos pixels;
        5 - mu: Parâmetro de suavização da convergência;
        6 - iter_max: Parâmetro de número máximo de iterações da descida;
        7 - eps: eps: Parâmetro de tolerância de gradiente.'''

    # Determinação do denoiser utilizado (non-local means da lib cv2)
    denoiser = cv2.fastNlMeansDenoising #bm3d.bm3d #

    # Inicializa variáveis necessárias no Domínio do tempo
    x0            = fft.ifft2(X).real
    si_x, si_y, n = Y.shape
    sx, sy        = shape
    s_fx          = fft.next_fast_len(sx)
    s_fy          = fft.next_fast_len(sy)

    # Inicia variável de avaliação da fç de custo
    lag_v = np.zeros(iter_max)
    jac_v = np.zeros(iter_max)
    jac   = 1
    tol   = 1e-5

    # OLHAR ISSO -> DIMS IGUAIS
    # Calcula TF de lbda * I(sx)
    lbda_mat = lbda * np.ones_like(X) #fft.fft2(np.eye(si_x)) #

    # Itera sobre o problema abordando-o pelo método de ponto fixo
    for ii in range(iter_max):
        print(f'\r{ii + 1}/{iter_max}', end='')

        # Calcula termo de ruído
        deno = denoiser(x0.astype(np.uint8), h=100.)
        #deno = denoiser(x0.astype(np.uint8), sigma_psd=0.15) #30/255)

        # Soma na razão termo com denoiser
        frac = lbda * fft.fft2(deno)

        # Soma termo constante ao denominador
        tmp1 = lbda_mat
        tmp2 = np.zeros_like(tmp1)

        # Itera por cada um dos canais observados
        for jj in range(n):
            # Soma termos relativos à canais e constantes de reg.
            tmp1 += mu * np.conj(K_vec[:, :, jj]) * K_vec[:, :, jj]
            tmp2 += mu * np.conj(K_vec[:, :, jj]) * Y[:, :, jj]

        # Divide pelo denominador encontrado
        frac += tmp2
        frac /= tmp1

        # Atualiza valores estimados para cada iteração
        x = fft.ifft2(frac).real

        # Zera termos não pertencentes ao dom. da img
        #x[sx:, sy:] = 0.
        #x[:sx, sy:] = 0.
        #x[sx:, :sy] = 0.

        # Regulariza os limites da imagem
        x  = img_norm(x).astype(np.float64)
        x /= x.max().max()

        # Calcula termos de ruído e iterações passadas
        n_term  = lbda * (x - deno)
        x0      = x
        X       = fft.fft2(x)

        # DEBUG
        if ii % 500 == 0:
            print()
            print(sx, sy)
            plt.imshow(x[:, :], cmap='gray')
            plt.axis('off')
            plt.title(f'Estimativa {ii+1}')
            plt.show()

        # Calcula Lagrangeano
        lag_v[ii]  = cost_fun(Y, K_vec, X, mu=mu) 
        lag_v[ii] += np.dot(x.T.ravel(), n_term.ravel())

        # Calcula módulo do jacobiano - TESTE
        jac_t = np.zeros_like(x)
        for jj in range(n):
            jac_t += 2 * mu * fft.ifft2(
                np.conj(K_vec[:, :, jj]) * (K_vec[:, :, jj] * X - Y[:, :, jj])
            ).real

        jac_t    += n_term
        jac       = linalg.norm(jac_t)
        jac_v[ii] = jac

    # Realiza plotagem para fins de debug
    if p_flag is True:
        plt.imshow(x[:, :], cmap='gray')
        plt.title(r'Estimativa de $\hat{x}$')
        plt.axis('off')
        plt.show()
        plt.semilogy(lag_v)
        plt.title(r'$\mathcal{L}$')
        plt.show()
        plt.semilogy(jac_v)
        plt.title(r'$|\nabla \mathcal{L}|$')
        plt.show()

    return X, n_term

def fixed_point_x_reg (X, K_vec, Y, shape, SNR=20, lbda=1e-3,
                   mu=1e-3, sig=1e-3, iter_max=1000, p_flag=False):
    '''Função de minimização da imagem latente x_hat a partir dos novos
       kerneis estimados. Para esta modelagem ustiliza-se do algoritmo 
       RED para sua implementação em ponto fixo, dessa forma, minimiza-se
               L(X, K, Y) + lbda * x^T(x - f(x)) + sig * ||x - f(x)||_2^2
       Relação de inputs:
        1 - x: Imagem latente inicial;
        2 - k: Tensor de kerneis (domínio dos pixels);
        3 - Y: Tensor de DFTs das imagens distorcidas
        4 - size_im: Tupla com dimensões da imagem no domínio dos pixels;
        5 - mu: Parâmetro de suavização da convergência;
        6 - iter_max: Parâmetro de número máximo de iterações da descida;
        7 - eps: eps: Parâmetro de tolerância de gradiente.'''

    # Determinação do denoiser utilizado (non-local means da lib cv2)
    denoiser = cv2.fastNlMeansDenoising #bm3d.bm3d #

    # Inicializa variáveis necessárias no Domínio do tempo
    x0            = fft.ifft2(X).real
    si_x, si_y, n = Y.shape
    sx, sy        = shape
    s_fx          = fft.next_fast_len(sx)
    s_fy          = fft.next_fast_len(sy)

    # calculo de constante associada a intensidade de ruido
    noi = np.exp(SNR / 20) ** 2

    # Inicia variável de avaliação da fç de custo
    lag_v = np.zeros(iter_max)
    jac_v = np.zeros(iter_max)
    jac   = 1
    tol   = 1e-5

    # OLHAR ISSO -> DIMS IGUAIS
    # Calcula TF de lbda * I(sx)
    lbda_mat = lbda * np.ones_like(X) #fft.fft2(np.eye(si_x)) #
    Feye     = np.ones_like(X)        #fft.fft2(np.eye(si_x)) #

    # Itera sobre o problema abordando-o pelo método de ponto fixo
    for ii in range(iter_max):
        print(f'\r{ii + 1}/{iter_max}', end='')

        # Calcula termo de ruído
        #deno = denoiser(x0.astype(np.uint8), sigma_psd=30/255) #0.15)
        deno = denoiser(x0.astype(np.uint8), h=100.)
        Deno = fft.fft2(deno)
        X0   = fft.fft2(x0)

        # Calcula termo relativo à const sig
        sig_term = sig * np.conj(
            Feye - Deno / X0         #Deno / X0     #
        ) * (X0 - Deno)              #Deno          #

        # Soma na razão termo com denoiser
        frac = lbda * Deno

        # Soma termo constante ao denominador
        tmp1 = lbda_mat
        tmp2 = sig_term         #np.zeros_like(tmp1) #

        # Itera por cada um dos canais observados
        for jj in range(n):
            # Soma termos relativos à canais e constantes de reg.
            tmp1 += (mu / noi) * np.conj(K_vec[:, :, jj]) * K_vec[:, :, jj]
            tmp2 += (mu / noi) * np.conj(K_vec[:, :, jj]) * Y[:, :, jj]

        # Divide pelo denominador encontrado
        frac += tmp2
        frac /= tmp1

        # Atualiza valores estimados para cada iteração
        x = fft.ifft2(frac).real

        ### Zera termos não pertencentes ao dom. da img
        #x[sx:, sy:] = 0.
        #x[:sx, sy:] = 0.
        #x[sx:, :sy] = 0.

        ### Regulariza os limites da imagem
        #x  = img_norm(x).astype(np.float64)
        #x /= x.max().max()

        # Calcula termos de ruído e iterações passadas
        n_term  = lbda * (x - deno)
        x0      = x
        X       = fft.fft2(x)

        # DEBUG
        if ii % 500 == 0:
            print()
            print(sx, sy)
            plt.imshow(x[:sx, :sy], cmap='gray')
            plt.axis('off')
            plt.title(f'Estimativa {ii+1}')
            plt.show()

        # Calcula Lagrangeano
        lag_v[ii]  = cost_fun(Y, K_vec, X, mu=mu) 
        lag_v[ii] += linalg.norm(x.T @ n_term)

        # Calcula módulo do jacobiano - TESTE
        jac_t = np.zeros_like(x)
        for jj in range(n):
            jac_t += 2 * mu * fft.ifft2(
                np.conj(K_vec[:, :, jj]) * (K_vec[:, :, jj] * X - Y[:, :, jj])
            ).real

        jac_t    += n_term
        jac       = linalg.norm(jac_t)
        jac_v[ii] = jac

    # Realiza plotagem para fins de debug
    if p_flag is True:
        plt.imshow(x[:sx, :sy], cmap='gray')
        plt.title(r'Estimativa de $\hat{x}$')
        plt.axis('off')
        plt.show()
        plt.semilogy(lag_v)
        plt.title(r'$\mathcal{L}$')
        plt.show()
        plt.semilogy(jac_v)
        plt.title(r'$|\nabla \mathcal{L}|$')
        plt.show()

    return X, n_term

def grad_des_x (X, K_vec, Y, size, lbda=1e-3, mu=1e-3, iter_max=1000, 
                p_flag=False):
    '''Função de minimização da imagem latente x_hat a partir dos novos
       kerneis estimados. Para esta modelagem ustiliza-se do algoritmo 
       RED para sua implementação de máxima descida, dessa forma, 
       minimiza-se:
               L(X, K, Y) + lbda * x^T(x - f(x)) -> f(): denoiser
       Relação de inputs:
        1 - x: Imagem latente inicial;
        2 - k: Tensor de kerneis (domínio dos pixels);
        3 - Y: Tensor de DFTs das imagens distorcidas
        4 - size_im: Tupla com dimensões da imagem no domínio dos pixels;
        5 - mu: mu: Parâmetro de suavização da convergência;
        6 - iter_max: Parâmetro de número máximo de iterações da descida;
        7 - eps: eps: Parâmetro de tolerância de gradiente.'''

    # Inicializa variáveis necessárias no Domínio do tempo
    x       = fft.ifft2(X).real
    l, _, n = Y.shape
    sx, sy  = size
    s_fx    = fft.next_fast_len(sx)
    s_fy    = fft.next_fast_len(sy)
    jac_v   = np.zeros(iter_max)
    lag_v   = np.zeros(iter_max)
    sig     = 0.5

    # Zera termos não pertencentes ao dom. da img
    #x[s_fx:, s_fy:] = 0.
    #x[:s_fx, s_fy:] = 0.
    #x[s_fx:, :s_fy] = 0.

    # Alocaçao de matrizes usadas ao longo do codigo
    KK = np.zeros_like(K_vec[:, :, 0])
    YY = np.zeros_like(Y[:, :, 0])

    # Inicializa nova estimativa de x
    grad   = np.zeros_like(x)
    n_term = np.zeros_like(x)

    # Declaraç~ao de funcional para facilitar uso do denoiser
    deno_f = lambda __x: cv2.fastNlMeansDenoising(__x.astype(np.uint8), h=15)
    #deno_f = lambda __x: bm3d.bm3d(__x.astype(np.uint8), sigma_psd=30/255)

    # Itera sobre o problema abordando-o pelo método de ponto fixo
    for ii in range(iter_max):
        print(f'\r{ii + 1}/{iter_max}', end='')

        # Inicializa nova estimativa de x
        grad = np.zeros_like(x)

        # Itera por cada um dos canais observados
        for jj in range(n):
            # Recupera termos de interesse p/ reconstrução
            KK, YY = K_vec[:, :, jj], Y[:, :, jj]

            # Calcula termos de erro de reconstrução para cada kernel
            grad += mu * fft.ifft2(
                np.conj(KK) * (KK * X - YY),
            ).real

        # Atualiza valores estimados para cada iteração
        f_x    = deno_f(x)
        n_term = lbda * (x - f_x) #.T @ (x.T @ (x - f_x)) # Alteração para correção do mismatch
        grad  += n_term

        # normaliza o gradiente
        #grad      = reg_vals(grad)
        n_jac     = linalg.norm(grad)
        jac_v[ii] = n_jac 
        #grad     /= n_jac

        # Calcula lagrangeano da fç para fins de debug
        lag_v[ii]  = reg_vals(cost_fun(Y, K_vec, X, mu=lbda))
        lag_v[ii] += np.dot(x.ravel(), n_term.ravel()) #linalg.norm(reg_vals(x.T @ n_term))

        # Calcula passo para descida
        sig = global_search_x(cost_fun, deno_f, Y, K_vec, X,
                                 -1 * grad, mu=mu, lbda=lbda, num=10) #num=5) #
        #sig = 0.5 / n_jac

        # Efetua passo de máxima descida
        x = x - sig * grad

        # Normalização da imagem e impressão de debug
        # x = img_norm_float(x)
        # x = 1 / (1 + np.exp(- x + 0.5))    # Normalização via sigmoid
        x = cv2.equalizeHist(img_norm(x))
        x = x.astype(np.float64) / 255
        #if ii % 10 == 0:
        #    print()
        #    print('max_min: ', x.min(), x.max())
        #    tmp = x  # cv2.equalizeHist(img_norm(x))
        #    plt.imshow(tmp[:, :], cmap='gray')
        #    plt.axis('off')
        #    plt.title(f'Estimativa {ii+1}')
        #    #plt.savefig(f'./material_reunioes/figs_12_jun_2023/it_{ii+1}.png')
        #    plt.show()

        # Atualiza valor de X
        X = fft.fft2(x)

    # Realiza plotagem para fins de debug
    if p_flag is True:
        plt.imshow(x[:, :], cmap='gray')
        plt.title(r'Estimativa de $\hat{x}$')
        plt.axis('off')
        plt.show()
        plt.semilogy(jac_v)
        plt.title(r'$\nabla_x \mathcal{L}$')
        plt.show()
        plt.semilogy(lag_v)
        plt.title(r'$\mathcal{L}$')
        plt.show()

    return X, lag_v, n_term

#-----------------------------------------------------
# Funções usadas em ambos os processos de minimização:
#-----------------------------------------------------

def reg_vals (Mat):
    '''Função de adequação de valores NaN e inf 
       em matrizes do tipo numpy.
       Relação de inputs:
        1 - Mat: Matriz com valores a serem regularizados.'''

    return np.nan_to_num(Mat, nan=0.0, posinf=1e10, neginf=-1e10)

#------------------------------------------------------------------
# Rotina de deconvolução em caso sem ruído através da otimização
# no domínio da frequência da equação:
#       min  ||Y_i - K_i^k X^k|| + lambda ||Y_j K_i^k - Y_i K_javaliado^k|| 
#------------------------------------------------------------------

def mult_decon_noiseless (y, n, K_hat=None, sk=4, eps=1e-4, iter_max=10_000, p_flag=True):
    '''Função de deconvolução de imagens usando as múltiplas
       saídas dos diversos canais gerados, seu resultado é uma 
       imagem latente obtida através da
       Relação de inputs:
        1 - y: Array multidimensional contendo imagens de todos os 
               canais
        2 - n: Números de canais disponíveis para o problema
        eps: Parâmetro de tolerância utilizado
        lamb: Parâmetro multiplicador da restrição de multicanal
        iter_max: Parâmetro com número máximo de iterações'''

    # calcula chute inicial para x_hat e salva as suas dimensões
    x_hat       = np.mean(y, axis=2) #+ 0.1 * im/linalg.norm(im))/2
    # ^ Observação de resultado... (hipótese: sensibilidade de ponto
    #   inicial). Ver se melhora com correção da convolução circular.
    x_hat      /= linalg.norm(x_hat)
    s_x, s_y, _ = y.shape
    s_fx        = fft.next_fast_len(sx)
    s_fy        = fft.next_fast_len(sy)
    k_hat       = np.zeros((s_x, s_y, n))
    
    # Inicializa variável de vetor de erro
    err_vec = np.zeros((n, iter_max))

    # Calcula as DFT's das grandezas envolvidas e inicializa o processo de otimização
    X_hat = fft.fft2(x_hat, (s_x, s_y))
    Y     = np.zeros((s_x, s_y, n), dtype=complex)
    for ii in range(n):
        Y[:, :, ii]     = fft.fft2(y[:, :, ii], (s_x, s_y))
    
    if isinstance(K_hat, type(None)):
        K_hat = np.zeros((s_x, s_y, n), dtype=complex)
        for ii in range(n):
            # TESTE
            ttmp            = gen_gaussian_kernel(sk, 1)
            ttmp2           = np.zeros_like(x_hat)
            ttmp2[:sk, :sk] = ttmp
            K_hat[:, :, ii] = fft.fft2(ttmp2)
            #K_hat[:, :, ii] = fft.fft2(np.ones_like(x_hat)) #np.divide(Y[:, :, ii], X_hat)

    # Cálculo da matriz de condição de parada inicial (basta ser > eps)
    cond = 10.0

    # Laço de descida de gradiente e otimização
    it = 0 
    while cond > eps and it < iter_max:
        # Otimização efetuada por cada componente de canal
        # Regulariza matrizes de entrada
        X_hat        = reg_vals(X_hat)
        tmp, err_vec, lag_v = bb_des_k(
            Y,
            K_hat,
            #X     = X_hat, #X_tmp,
            sk       = sk,
            lbda     = 1,
            mu_0     = 0,
            iter_max = 100_000,
            p_flag   = False
        )
        #tmp, err_vec, lag_v = grad_des_k(
        #    Y,
        #    K_hat,
        #    #X        = X_hat, #X_tmp,
        #    sk       = sk,
        #    lbda     = 1,
        #    iter_max = 100_000, #100_000,
        #    p_flag   = False
        #)

        for ii in range(n):
            K_hat[:, :, ii] = reg_vals(tmp[:, :, ii])

        # Impressão de variáveis de controle para cada iteração.
        if p_flag:
            print(f'Grandezas da {it + 1}a iteração')
            for idx in range(n):
                tmp_k = fft.ifft2(K_hat[:, :, idx]).real
                fftX  = np.divide(Y[:, :, idx], K_hat[:, :, idx] + 0.001)
                x_tmp = fft.ifft2(fftX).real

                fig, ((ax1, ax2), (ax4, ax3)) = plt.subplots(2, 2)
                ax1.imshow(ker[:sk, :sk, idx], cmap='gray')
                ax1.axis('off')
                ax1.set_title(f'Kernel original {idx + 1}')
                ax2.imshow(tmp_k[:sk, :sk] / tmp_k.sum().sum(), cmap='gray')
                ax2.axis('off')
                ax2.set_title(f'Kernel estimado {idx + 1}')
                ax3.imshow(x_tmp[:s_fx, :s_fy], cmap='gray')
                ax3.axis('off')
                ax3.set_title(f'Imagem estimada {idx + 1}')
                ax4.imshow(blured_im[:s_fx, :s_fy, idx], cmap='gray')
                ax4.axis('off')
                ax4.set_title(f'Imagem borrada {idx + 1}')
                fig.colorbar(None, ax=[ax1, ax2, ax3, ax4])
                plt.show()

                plt.semilogy(lag_v)
                plt.ylabel('$\mathcal{L}$ semilog')
                plt.xlabel('Iteração')
                plt.show()

            #print(f"Canais de minimização bem sucedida:")
            #print(f"PSNR: {calc_psnr(im, x_tmp)}")
            #print(f"condição de minimização: {np.round(linalg.norm(cond), 3)}")
            #print("---------------------------")

        # Atualiza a condição
        cond = 0
        for ii in range(n):
            cond += linalg.norm(cost_fun_jac(Y, K_hat, mu=1))

        # Incrementa o índice de iteração
        it  += 1

    # Transporta a imagem latente e os Kerneis para o domínio dos pixels
    x_hat = np.zeros_like(x_hat)
    print(f'condição de minimização: {np.round(linalg.norm(cond), 3)}')
    for ii in range(n):
        fftX            = np.divide(Y[:, :, ii], K_hat[:, :, ii] + 0.001)
        x_hat          += fft.ifft2(fftX).real / n
        k_hat[:, :, ii] = fft.ifft2(K_hat[:, :, ii]).real

    return img_norm(x_hat), k_hat, err_vec, lag_v

#----------------------------------------------------
# Funções usadas para o prcesso com modelagem ruidosa
#----------------------------------------------------

def plot_vars (x_hat, k_hat, ker, im, err_ker, err_img, lag_v, sk=4):
    # Adquire número de canais
    _, _, n = k_hat.shape

    # Plota resultados obtidos para kerneis
    map = cm.ScalarMappable(norm=None, cmap='gray')
    for ii in range(n):
        fig, (ax1, ax2) = plt.subplots(1, 2)
        ax1.imshow(k_hat[:sk, :sk, ii], cmap='gray')
        ax1.set_title(f'Kernel estimado {ii+1}')
        ax1.axis('off')
        ax2.imshow(ker[:sk, :sk, ii], cmap='gray')
        ax2.set_title(f'Kernel de referência {ii+1}')
        ax2.axis('off')
        fig.colorbar(map, ax=[ax1, ax2])
        plt.show()

    sx, sy      = x_hat.shape
    s_fx        = fft.next_fast_len(sx)
    s_fy        = fft.next_fast_len(sy)

    # Plota resultados obtidos para a estimativa de X_hat
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.imshow(x_hat[:s_fx, :s_fy], cmap='gray')
    ax1.set_title(r'Imagem estimada $\hat{x}$')
    ax1.axis('off')
    ax2.imshow(im[:s_fx, :s_fy], cmap='gray')
    ax2.set_title(r'Imagem original')
    ax2.axis('off')
    fig.colorbar(map, ax=[ax1, ax2])
    plt.show()

    # Plota variáveis de erro por iteração
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(15, 10))
    ax1.semilogy(err_ker)
    ax1.set_title('Erro dos kerneis estimados')
    ax2.semilogy(err_img)
    ax2.set_title('Erro da imagem estimada')
    fig.show()

    # Plota valor da fç lagrangeana para cada iteraç~ao
    plt.figure(figsize=(15, 10))
    plt.semilogy(lag_v)
    plt.title(r'$\mathcal{L}$ x iter')
    plt.xlabel('Iteração')
    plt.ylabel(r'$\mathcal{L}$')
    plt.show()

def deconv_noise (X_hat, K_hat, Y, shape, lbda, mu, num,
                  int_iter, ker, im, sk=4, p_flag=True):
    '''Função utilizada para a reconstrução tanto dos kerneis
       quanto da imagem em si. Aqui a abordagem proposta consiste
       na otimização alternativa sobre o espaço dos kerneis e
       o espaço da imagem reconstruída.
       Relação de inputs:
        1 - X_hat:    TF da reconstrução inicial
        2 - K_hat:    TF dos kerneis inicial
        3 - lbda:     Constante de regularização do termo RED
        4 - mu:       Constante de regularização do erro de rec.
        5 - num:      Número de iterações do laço externo
        6 - int_iter: Número de iteraações dos laços internos
        7 - ker:      Estrutura com kernels legítimos
        8 - im:       Estrutura com imagem legítima
        9 - p_flag:   Flag de plotagem de dados (p/ debug)'''

    # Criação  de estruturas de retorno
    x_hat   = np.zeros(X_hat.shape)
    k_hat   = np.zeros(K_hat.shape)
    _, _, n = k_hat.shape

    # Criação de estruturas p/ armazenamento e plotagem do erro
    err_ker = np.zeros(num)
    err_img = np.zeros(num)
    lag_v   = np.zeros(num * int_iter) #20)

    # Loop principal da otimização e recuperação do sinal
    for it in range(num):
        # Aplica x-step
        print(f'\nX-step {it+1} / {num}:')
        #X_hat, n_term = fixed_point_x(
        X_hat, lag_v[it*int_iter:(it*int_iter + int_iter)], n_term = grad_des_x(
            X_hat,
            K_hat,   #Ker,
            Y,
            shape,
            lbda     = lbda, #1e-3,     # 30dB
            mu       = mu,
            iter_max =  int_iter, # 20,
            p_flag   = p_flag
        )

        # Teste
        ttmp               = fft.ifft2(X_hat).real
        ttmp[shape[0]:, :] = 0.0
        ttmp[:, shape[1]:] = 0.0
        X_hat              = fft.fft2(ttmp)

        # Aplica k-step
        print(f'\nK-step {it+1} / {num}:')
        tmp, _, _ = grad_des_k(
            Y,
            K_hat,
            X        = X_hat, #None,
            sk       = sk,
            lbda     = mu,    #1e-3,    # 30dB
            iter_max = int_iter,    # DEBUG 
            p_flag   = p_flag
        )

        for ii in range(n):
            K_hat[:, :, ii]   = reg_vals(tmp[:, :, ii])
            tttmp             = fft.ifft2(K_hat[:, :, ii]).real
            tttmp[sk:, :]     = 0.0
            tttmp[:, sk:]     = 0.0
            tttmp            /= np.sum(tttmp.sum())
            K_hat[:, :, ii]   = fft.fft2(tttmp)

        # Atualiza valores dos erros p/ kernel
        for ii in range(n):
            k_hat[:, :, ii] = fft.ifft2(K_hat[:, :, ii]).real
            tmp             = k_hat[:sk, :sk, ii].ravel() - ker[:sk, :sk, ii].ravel()
            err_ker[it]    += linalg.norm(tmp) ** 2

        err_ker[it] = np.sqrt(err_ker[it])

        # Atualiza valores dos erros p/ imagem
        sx, sy      = im.shape
        x_hat       = fft.ifft2(X_hat).real
        tmp2        = x_hat[:sx, :sy].ravel() - im[:sx, :sy].ravel()
        err_img[it] = linalg.norm(tmp2)

        # Determina valor da fç de custo p/ cada iteração
        lag_v[it]  = cost_fun(Y, K_hat, X_hat, mu=mu) 
        lag_v[it] += linalg.norm(x_hat.T @ n_term)

    # Ciclo de plotagem (habilitado por p_flag)
    if p_flag is True:
        plot_vars(x_hat, k_hat, ker, im, err_ker, err_img, lag_v, sk=sk)
        print(np.min(lag_v))

    # Retorna estruturas estimadas
    return img_norm(x_hat), k_hat, lag_v[it]
