---
title: Deconvolução por Multicanal em Sinais Ruidosos
author: João Rabello Alvim
theme: Berlin
bibliography: bibliografia.bib
supress-bibliography: true
endnote: true
toc: true
toc-depth: 3
---

# Resultados interessantes

## Erros RMS

![Erros RMS nos dois domínio de interesse](./figs_20_mar_23/rms.png){ width=250px }

## Função de custo

![Função de custo durante processo](./figs_20_mar_23/cost.png){ width=250px }

## Saídas no início do processo

![Iteração 1 - k1](./figs_20_mar_23/k1_it_1.png){ width=100px }

## Saídas no início do processo

![Iteração 1 - k2](./figs_20_mar_23/k2_it_1.png){ width=100px }

## Saídas no início do processo

![Iteração 1 - $\hat{x}$](./figs_20_mar_23/x_it_1.png){ width=100px }

## Saídas no ótimo dos kernels

![Iteração 1 - k1](./figs_20_mar_23/k1_it_8.png){ width=100px }

## Saídas no ótimo dos kernels

![Iteração 1 - k2](./figs_20_mar_23/k2_it_8.png){ width=100px }

## Saídas no ótimo dos kernels

![Iteração 1 - $\hat{x}$](./figs_20_mar_23/x_it_8.png){ width=100px }

## Saídas no ótimo da imagem

![Iteração 1 - k1](./figs_20_mar_23/k1_it_17.png){ width=100px }

## Saídas no ótimo da imagem

![Iteração 1 - k2](./figs_20_mar_23/k2_it_17.png){ width=100px }

## Saídas no ótimo da imagem

![Iteração 1 - $\hat{x}$](./figs_20_mar_23/x_it_17.png){ width=100px }

## Saídas no fim do processo

![Iteração 1 - k1](./figs_20_mar_23/k1_final.png){ width=250px }

## Saídas no fim do processo

![Iteração 1 - k2](./figs_20_mar_23/k2_final.png){ width=250px }

## Saídas no fim do processo

![Iteração 1 - $\hat{x}$](./figs_20_mar_23/x_final.png){ width=250 }

