---
title: Resultados e discussões com dados reais
author: João Rabello Alvim
theme: Frankfurt
bibliography: bibliografia.bib
lang: pt-BR
supress-bibliography: true
endnote: true
toc: true
toc-depth: 1
---

# Contexto da produção desenvolvida

## Em que ponto estamos?

- Testando a solução de deconvolução em dados reais
- Observando os efeitos da abordagem alternada nestes cenários
- Como o RED reage para estas imagens
- usando os arquivos de `data_set3` obtido neste [link](http://zoi.utia.cas.cz/deconvolution.html)

## Em que ponto estamos?

### Imagem borrada 1

![Imagem obtida a partir de captura de longa exposição e com distorção por movimento](../data_set3/input1.jpg){ width=250px }

## Em que ponto estamos?

### *Kernel* de borramento 1

![Borrão associado ao movimento sobre a **Imagem 1**](../data_set3/psf1.png){ width=100px }

## Em que ponto estamos?

### Imagem borrada 2

![Imagem obtida a partir de captura de longa exposição e com distorção por movimento](../data_set3/input2.jpg){ width=250px }

## Em que ponto estamos?

### *Kernel* de borramento 2

![Borrão associado ao movimento sobre a **Imagem 2**](../data_set3/psf2.png){ width=100px }

# Testes e propostas

## Passo $x$

### Dificuldades iniciais:

- Os valores padrão das funções `grad_des_x` e `global_search_x` dificilmente convergem;
- Solução: ajuste fino para espaço de busca! 
- Mudar intervalo de busca em `global_search_x`:
	- Intervalo antigo: $[0,05; 1,0]$
	- Intervalo novo: $[0,0; 1 \cdot 10^{-10}]$

## Passo $x$

### Dificuldades iniciais

- Inicialmente, verificou-se a saída da função `grad_des_x` com os *kernels* originais (Figuras 2 e 4)
- No entanto, percebeu-se o efeito progressivo de compressão das cores

## Passo $x$

### Dificuldades iniciais

![Progressão das iterações para a saída da função `grad_des_x`](./figs_12_jun_2023/it_31.png){ width=250px }

## Passo $x$

### Dificuldades iniciais

![Progressão das iterações para a saída da função `grad_des_x`](./figs_12_jun_2023/it_61.png){ width=250px }

## Passo $x$

### Dificuldades iniciais

![Progressão das iterações para a saída da função `grad_des_x`](./figs_12_jun_2023/it_91.png){ width=250px }

## Passo $x$

### Dificuldades iniciais

- Para resolver este problema, foram pensadas duas soluções:
	1. Normalização da imagem após cada iteração
	1. Equalização do histograma após cada iteração

\phantom{uuuuui}

- A segunda alternativa foi mais bem sucedida, mas ainda sim, não resolve o problema completamente...
- Curiosamente, usando a abordagem alternada as cores não ficam chapadas.

## Passo $k$

### Dificuldades iniciais

Para o contexto da da otimização em `k` foram percebidas duas dificuldades iniciais:

1. Inicialização dos *kernels*
1. Promover condição de esparsidade

## Passo `k`

### Inicialização dos *kernels*

Para resolver o problema 1, propõe-se a inicialização

\phantom{uuuui}

![Proposta de inicialização para o *kernel* 1](../data_set3/prior_1.png){ width=100px }

## Passo `k`

### Inicialização dos *kernels*

Para resolver o problema 1, propõe-se a inicialização

\phantom{uuuui}

![Proposta de inicialização para o *kernel* 2](../data_set3/prior_2.png){ width=100px }

## Passo `k`

### Promover condição de esparsidade

Como o borrão das imagens está associado com movimento, os seus *kernels* apresentam esparsidade como característica, dessa forma, propõe-se uma regularização da norma $\ell_1$, dentro da função `grad_des_k`. Na prática, fazemos:

\phantom{uuuui}

\begin{equation}
	\nabla \mathcal{L}_k \mathrel{+}= 1 \cdot 10^{-5} \mathrm{sign} \{\vec{k}\}
\end{equation}

\phantom{uuuui}

O valor $1 \cdot 10^{-5}$ funciona como constante de regularização.

# Resultados

## Resultados minimização alternada

- Serão apresentados aqui os resultados obtidos com a regularização da norma $\ell_1$ com caráter alternado:
	- 20 iterações para o $x$-step
	- 500 iterações para o $k$-step

## Resultados minimização alternada

### Fim do primeiro $x$-step

![Teste 1](./figs_12_jun_2023/out_x_it_1.png){ width=200px }

## Resultados minimização alternada

### Fim do primeiro $k$-step

![Teste 1](./figs_12_jun_2023/out_k_it_1.png){ width=250px }

## Resultados minimização alternada

### Fim do quinto $x$-step

![Teste 1](./figs_12_jun_2023/out_x_it_5.png){ width=200px }

## Resultados minimização alternada

### Fim do quinto $k$-step

![Teste 1](./figs_12_jun_2023/out_k_it_5.png){ width=250px }

## Resultados minimização alternada

### Fim do décimo $x$-step

![Teste 1](./figs_12_jun_2023/out_x_it_10.png){ width=200px }

## Resultados minimização alternada

### Fim do décimo $k$-step

![Teste 1](./figs_12_jun_2023/out_k_it_10.png){ width=250px }

# Conclusões

## Conclusões gerais

- A otimização no domínio $x$ tem conseguido melhorar o *blur* da imagem original
- Ainda é necessário melhorar a estimativa em no $k$-step
	- Seria a regularização da norma $\ell_1$ a melhor forma de promover a esparsidade esperada?
	- Como otimizar o processo?
	- Seria interessante encontrar um novo algoritmo de busca de passo (armijo)?
