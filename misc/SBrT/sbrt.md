---
title: Multichannel Blind Deconvololution for Noisy Measurements
date: 2023
author:
	- João Rabello Alvim
	- Kenji Nose-filho
	- Renato da Rocha Lopes
affiliation: Simpósio Brasileiro de Telecomunicações e Processamento de Sinais - SBrT
theme: Madrid
endnote: true
toc: true
toc-depth: 3
---

# Introduction

## Inverse Problems

Considering a given image $y$, a channel $k$ and some statistical noise $n$, one can notice:

\begin{align}
	\begin{split}
		y(i, j) &= (x * k) (i, j) + n(i, j) \\
		&= \sum_l \sum_k x(l, k) k(i-l, j-k) + n(i, j)
	\end{split}
	\label{eq:inv}
\end{align}

Finding the signal $y$ given an arbitrary image $x$, some channel $k$ and noise $n$ is to solve the **direct** form of the problem

## Multiple Channels

![SIMO representation for multiple aquisitions of the same image](./assets/SIMO_model.png){ width=300px }

## Motivation

Multichannel Blind Deconvolution (MBD) can be used in a wide range of different fields, such as:

- Microscopical imaging

- Satellite imaging

- Sensor array systems (camera systems)

<!--## Inverse Problems

Considering a given image $y$, a channel $k$ and some statistical noise $n$, one can notice:

\begin{align}
	\begin{split}
		y(i, j) &= (x * k) (i, j) + n(i, j) \\
		&= \sum_l \sum_k x(l, k) k(i-l, j-k) + n(i, j)
	\end{split}
	\label{eq:inv}
\end{align}

Finding the signal $y$ given an arbitrary image $x$, some channel $k$ and noise $n$ is to solve the **direct** form of the problem -->

<!-- ## Inverse Problems

* Finding $x$ from the measurement $y$ in (\ref{eq:inv}) is an **inverse problem**

* A deconvolution inverse problem can be extremely **ill-posed**

* To deal with this problem, one can use **regularization techniques**

* In this work, the solution will be based on *multichannel deconvolution* -->

<!-- ## Inverse Problems

![Representation of direct and indirect flux for a problem](./assets/inverse_prob.png){ width=350px } -->

<!-- ## Multiple Channels

In some applications, the same image can be captured by multiple sensors

$$
	\begin{cases}
		y_1 = k_1 * x \\
		\quad\quad\vdots \\
		y_i = k_i * x \\
		\quad\quad\vdots \\
		y_m = k_m * x \\
	\end{cases}
$$

This model is known by SIMO (*Single Input Multiple Output*) -->


<!-- The proposed approach consists in combining MBD with the Regularization by Denoising (RED) technique -->

<!-- ## Motivation

- Any captured image will be distorted by an usually unknown channel (or kernel)

- In order to retrieve the original signal, some deconvolution technique should be applied over the measurement

- This is an **inverse problem** -->

<!-- PENSAR EM MAIS COISAS PRA FALAR NA MOTIVAÇÃO! -->

<!-- ## Inverse Problems

### Ill-posedness

Following Haddamard definition, a well posed problem needs to satisfy the following conditions:

\phantom{uuuuui}

1. There must **exist** a solution to the problem

1. This solution must be **unique**

1. The solution must depend **continuously** to the input

\phantom{uuuuui}

If one of this conditions isn't satisfied, the problem is then **ill-posed** \cite{HadamardSurLP} -->

## Motivation

- MBD can recover signals without prior information, but is extremely **ill-posed**

- RED is a regularization technique, but is supervised in nature

- Why not combine both?

## Problem Setup

For the synthetic data tests, the following images were used:

![Reference (original) iamge](./figs/starfish_pb.png){ width=125px }

## Problem Setup

![Blurred image and randomly generated blurring kernel 1](./figs/kernel_rand1.png){ width=350px }

## Problem Setup

![Blurred image and randomly generated blurring kernel 2](./figs/kernel_rand2.png){ width=350px }

## Problem Setup

![Blurred image and simplified kernel 1](./figs/kernel_simp1.png){ width=350px }

## Problem Setup

![Blurred image and simplified kernel 2](./figs/kernel_simp2.png){ width=350px }

<!-- ## Convolution Through Multiple Channels

Given two channels $k_i, k_j \in \{k_1, \dots, k_m\}$ and a reference signal $x$. One can build the following system:

\begin{align*}
	y_i &= k_i * x \\
	y_j &= k_j * x
\end{align*} -->

<!-- # Mathematical Model -->

<!-- ## Convolution Through Multiple Channels

Knowing that the convolution operation is **commutative**, one can notice that:

\begin{align*}
	y_i * k_j &= (k_i * x) * k_j \\
	&= k_i * (x * k_j) \\
	&= k_i * y_j \\
	&= y_j * k_i \\
\end{align*}

Which implies: $y_i * k_j - y_j * k_i = 0$ -->

# Mathematical Model

## Convolution Through Multiple Channels

Knowing that the convolution operation is **commutative** one can see that $y_i * k_j = y_j * k_i$, and build the following cost function:

\phantom{uuuuui}

### Cost Function

\begin{equation}
	\min_{x, k_i} \mathcal{L} (x, \vec{k}, \vec{y}) = \sum_{j \neq i} ||y_j * k_i - y_i * k_j||^2_2
	\label{eq:lag} 
\end{equation}

<!-- A condição de identificabilidade deste problema é assegurada quando $k_i$ e $k_j$ não compartilham zeros em comum, ou seja, ambas as transformadas $Z$ do sistema não possuem ambiguidade em seus polinômios. Visto que qualquer combinação linear de $k_i$ e $k_j$ também compartilhará o mesmo zero. -->

<!-- ## Mathematical Model

Nevertheless, one can notice that, by modeling the following matrix...

\begin{equation*}
	\mathbb{Y} = 
	\begin{bmatrix}
		C_{y_2} & C_{y_3} & \dots & C_{y_m} &  & & & & \\
		-C_{y_1} & & & &  C_{y_3} & \dots & C_{y_m} & & \\
		& -C_{y_1} & & & -C_{y_2} & & & \dots & \\
		& & \ddots & & & \ddots & & & C_{y_m} \\
		& & & -C_{y_1} & & & -C_{y_2} & & -C_{y_{m-1}}
	\end{bmatrix}^T
	\label{eq:mat}
\end{equation*} -->

<!-- ## Mathematical Model

... solving (\ref{eq:lag}) is equivalent to the following least squares problem:

\begin{align}
	\begin{split}
		\min &\quad \vec{k}^T \mathbb{Y}^T \mathbb{Y} \vec{k} \\
		\text{s.t.} &\quad ||\vec{k}||_2 = 1
	\end{split}
	\label{eq:quad_prob}
\end{align}

Moreover solving (\ref{eq:quad_prob}) is a rather **expensive** task -->

<!-- ## Mathematical Model

- To reduce complexity, we can use the **convolution theorem** and perform these operations in the frequency domain

- To better pose the problem in terms of number of possible solutions, **regularization techniques** are required -->

<!-- ## Noise and Regularization

Since the signal reconstruction is also important for deconvolution purposes, the following term can be summed to (\ref{eq:lag}):

\phantom{uuuui}

### Reconstruction Error Regularization

- Minimizes the reconstruction error

$$
	\frac{\mu}{2} ||x * k_i - y_i||_2^2
$$ -->

## Noise and Regularization

RED is based in the assumption that the signal and noise subspaces are **orthogonal** to each other

\phantom{uuuui}

### Regularization by Denoising (RED)

- Promotes solutions orthogonal to the denoiser residue

\begin{align*}
	&\lambda x^T\big(x - f(x)\big) \\
	&\frac{\partial}{\partial x} \big(x^T(x - f(x))\big) \approx x - f(x)
\end{align*}

<!-- ## Noise and Regularization

In order to a denoiser $f(\cdot)$ be able to perform RED, it must satisfy two conditions:

\phantom{uuuui}

1. Local Homogeneity: $f(x) = \nabla_x f(x) \cdot x$

1. (Strong) Passivity: $\rho(f) \leq 1 \implies \rho(\nabla_x f) ||x|| \leq ||x||$, where $\rho(f)$ is the spectrum range of the denoiser

<!-- ## Noise and Regularization

The RED algorithm can be evaluated in three distinct ways

\phantom{uuuui}

1. Steepest Descent

1. Fixed Point

1. ADMM -->

# Methodology and Results

## Proposed Implementation

### Optimization Setup - Noiseless Scenario

* Steepest descent over eq. (\ref{eq:lag})

* Software implementation in the `Python` programming language

* 100E$^3$ descent iterations

* Comparison between line search methods:

	1. Global Search

	1. Barzilai-Browein

	1. Fixed Step

- The image reconstruction is obtained by $$\hat{x} = \mathcal{F}^{-1}\Bigg\{ \frac{Y_i}{K_i} \Bigg\}$$

<!-- ## Gradient Descent

- In order to find the solutions $\hat{x}$ and $\hat{k_i}$, we must perform **gradient descent**
- The solutions are found iteratively by choosing the **steepest descent direction** ($\nabla_x \mathcal{L}$ or $\nabla_{k_i} \mathcal{L}$)

\begin{align*}
	x^{l} = x^{l-1} - \sigma \nabla_x \mathcal{L} \\
	k_i^{l} = k_i^{l-1} - \sigma \nabla_k{_i} \mathcal{L}
\end{align*}

\phantom{uuuui}

In which $\sigma \in \mathbb{R}$ is a constant calculated by a given line search technique and $\{x^l\}_{l=1}^\infty \rightarrow \hat{x}$ as well as $\{k_i^l\}_{l=1}^\infty \rightarrow \hat{k_i}$ -->

## Gradient Descent

- To find the steepest descent direction, we need some way to calculate the **cost function's gradient**
- To reduce the overall computational cost, the differentiation is performed in the **frequency domain**

\begin{equation*}
	\nabla_{k_i} \mathcal{L} = 2 \sum_{i \neq j} \mathcal{F}^{-1}\{Y_j^* \cdot (K_i \cdot Y_j - K_j \cdot Y_i) \}
	%&+ \mu \sum_{i=1}^m \mathcal{F}^{-1} \big\{X^* \cdot (X \cdot K_i - Y_i) \big\} \\
	%\nabla_{k_j} \mathcal{L} &= 2 \sum_{i \neq j} \mathcal{F}^{-1}\{-Y_i^* \cdot (K_i \cdot Y_j - K_j \cdot Y_i)\} \\
\end{equation*}

<!-- \phantom{uuuui}

Where $\mathcal{F} \{ \cdot \}$ represents the **Fourier transform** of a given signal. -->

## Global Search ($k$-step)

- The global search approach consists in the partition of the interval $[a, b]$ in $\delta$ segments. The step $\sigma = j\frac{b-a}{\delta}$ where $j \in [0, 1, \dots, \delta - 1]$.

- The optimal index $j$ (which minimizes $\mathcal{L}$)

- \textcolor{red}{Cons}: the cost function is evaluated $\delta$ times

## Barzilai-Browein Method ($k$-step)

- Its a Quasi-Newton method which provides a first order approximation to the hessian matrix
- \textcolor{green}{Pro}: Doesn't need to evaluate $\mathcal{L}$, only =$\nabla_k \mathcal{L}(k^l)$, $\nabla_k \mathcal{L}(k^{l-1})$ and it's past values

\begin{align}
	\sigma^\prime &= \frac{<\mathrm{vec}(\Delta k), \mathrm{vec}(\Delta g)>}{<\mathrm{vec}(\Delta g), \mathrm{vec}(\Delta g)>} \\
	\sigma^{\prime\prime} &= \frac{<\mathrm{vec}(\Delta k), \mathrm{vec}(\Delta k)>}{<\mathrm{vec}(\Delta k), \mathrm{vec}(\Delta g)>}
	\label{eq:dir_b_2}
\end{align}

-   The method outputs two different step sizes $\sigma^\prime$ and $\sigma^{\prime \prime}$ that can be use interchangeably

## Barzilai-Browein Method ($k$-step)

- The vectors $\Delta x$ and $\Delta g$ are calculated by the following expressions:

\begin{align*}
	\Delta k &= k^l - k^{l-1} \\
	\Delta g &= \nabla_k \mathcal{L}(k^l) - \nabla_k \mathcal{L}(k^{l-1})
\end{align*}

## Fixed Step ($k$-step)

We use a given fixed step size $\sigma \in \mathbb{R}$ pondered by a specific constant (for the purpose of this research, we use the inverse of the gradient at that point)

<!-- COLOCAR AQUI RESULTADOS NOISELESS -->
## Results (noiseless scenario)

\centering

![Mean Squred reconstruction method for each line search for the $k$-step](./figs/rmse.png){ width=250px }

## Results (noiseless scenario)

\centering

![Outcome for Kernel 1 (fixed step)](./figs/k1_bicanal.png){ width=250px }

## Results (noiseless scenario)

\centering

![Outcome for Kernel 2 (fixed step)](./figs/k2_bicanal.png){ width=250px }

## Results (noiseless scenario)

\centering

![Outcome for Kernel 1 (global search)](./figs/ker_bb_a.png){ width=250px }

## Results (noiseless scenario)

\centering

![Outcome for Kernel 2 (global search)](./figs/ker_bb_b.png){ width=250px }

## Results (noiseless scenario)

\centering

![Outcome for Kernel 1 (Barzilai-Browein)](./figs/k1_bicanal_bb.png){ width=250px }

## Results (noiseless scenario)

\centering

![Outcome for Kernel 2 (Barzilai-Browein)](./figs/k2_bicanal_bb.png){ width=250px }

## Augmented Cost Function

Now, with the different regularizing terms, the augmented cost function becomes:

\begin{align*}
	\min_{x, k_i} \mathcal{L} (x, \vec{k}, \vec{y}) &= \overbrace{\sum_{j=1}^m ||y_j * k_i - y_i * k_j||^2_2}^{\text{multichannel criterion}} + \\
	&+ \underbrace{\frac{\mu}{2} || x * k_i - y_i ||_2^2}_{\text{Reconstruction error}} + \underbrace{\lambda x^T (x - f(x))}_{\text{RED}}
	\label{eq:lag_aug} 
\end{align*}

## Fixed Point RED ($x$-step)

By detecting the fixed point (where $\nabla_x \mathcal{L} = 0$), the RED conditions and assuming $x^l \perp \big(x^{l-1} - f(x^{l-1})\big)$, one can find the following iterative formula for $x^ l$:

\begin{equation}
x^l = \mathcal{F}^{-1} \Bigg\{ \frac{\lambda \mathcal{F}\{f(x^{l-1})\} + 2 \mu \sum_{i=1}^m K_i^* Y_i}{\lambda + 2 \mu \sum_{i=1}^m K_i^* K_i} \Bigg\}
\end{equation}

## Proposed Implementation

### Optimization Setup - Noisy Scenario

* Alternated optimization between $x$ and $k_i$ domains

* $\lambda = 0.5$ and $\mu = 1.0$

* We use the **fixed point RED** for the $x$-step optimization

* 20 macro-iterations and 500 micro-iterations (each domain)
	- $100 \cdot 10^3$ iterations in total

* Images with 30 dB synthetic AWGN

## Results (noisy scenario)

\centering

![MSE of estimates in 30 dB SNR regimen (``simplified'' kernels)](./figs/RMS_simp.png){ width=350px }

## Results (noisy scenario)

\centering

![Cost function per iteration for in 30 dB SNR regimen (``simplified'' kernels)](./figs/Cost_simp.png){ width=350px }

## Results (noisy scenario)

\centering

![Obtained reconstructions from the image and the simplified kernels](./figs/ker_simp.png){ width=300px }

## Results (noisy scenario)

\centering

![MSE of estimates in 30 dB SNR regimen (random kernels)](./figs/RMS_rand.png){ width=350px }

## Results (noisy scenario)

\centering

![Cost function per iteration for in 30 dB SNR regimen (random kernels)](./figs/Cost_rand.png){ width=350px }

## Results (noisy scenario)

\centering

![Obtained reconstructions from the image and the random kernels](./figs/ker_rand.png){ width=300px }

# Conclusions

## Conclusions

- The line search algorithm is key for good behavior of the deconvovlution system

- Simpler line-searches can perform as good as more complex techniques

- Good estimates for $x$, but not that good for $k_i$...
	- Regularize $k_i$? Application specific
	- Good initializations for $k_i$

## Conclusions

- When dealing with noisy inputs, some regularization technique is **mandatory**
	- The RED method can provide a solution for this need
	- RED in fact composes a good solution

<!-- ### Perspectivas futuras

- Acertar valor ótimo das constantes $\lambda$ e $\mu$ para cenários de baixa SNR

- Verificar possíveis regularizações para $k_i$ -->

##

\centering
\Large \textbf{Thank you for the attention!!!}

## References

\centering

![](./figs/Ref_img_1.png){ width=250px }

## References

\centering

![](./figs/Ref_img_2.png){ width=250px }

# Bonus section

## Tests with real data

![inputs to the database avaliable through this [link](http://zoi.utia.cas.cz/deconvolution.html)](./figs/input_real.png){ width=350px }

## Tests with real data

- RMSE = $0.306$ and PSNR = $5.14$ dB

![Outcome with supervised RED](./figs/output_real_red.png){ width=250px }

## Tests with real data

- RMSE = $6.95 \cdot 10^{-13}$ and PSNR = $121.58$ dB

![Outcome with our approach](./figs/output_real_our.png){ width=250px }
