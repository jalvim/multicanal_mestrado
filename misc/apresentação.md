---
title: Deconvolução por Multicanal em Sinais Ruidosos
author: João Rabello Alvim
theme: Berlin
bibliography: bibliografia.bib
supress-bibliography: true
endnote: true
toc: true
toc-depth: 3
---

# Introdução

## Motivação

Para aplicações de processamento de imagens e de sinais, de uma forma geral, os dados e sinais avaliados apresentam maior ou menor grau de interferência à ruído: seja este inserido pelos sensores como dentro do próprio circuito utilizado para a aquisição das imagens.

Dessa forma, é importante utilizar a expertise acumulada de anos em pesquisa sobre sistemas de estimação e extração de ruído (*denoiser*). Uma forma interessante de contornar este problema é a partir da regularização RED (**RE***gularization by* **D***enoising*), proposto em \cite{romano_little_2017}.

## Equação utilizada

\begin{align*}
	\min_{x, k_i} \mathcal{L} (x, \vec{k}, \vec{y}) &= \overbrace{\sum_{j=1}^m ||y_j * k_i - y_i * k_j||^2_2}^{\text{multicanal}} + \\
	&+ \underbrace{\frac{\mu}{2} || x * k_i - y_i ||_2^2}_{\text{erro}} + \overbrace{\frac{\lambda}{2} x^T (x - f(x))}^{\text{RED}}
	\label{eq:lag} 
\end{align*}

## Restrições do problema

Trabalhar com uma modelagem multicanal implica em alguns problemas mais imediatos: 

1. Como tratar as variáveis de interesse $\hat{x}$ e $\vec{k}$
1. Em que conjunto de variáveis aplicar o *denoiser*
1. Quais métodos utilizar em cada etapa

## Como tratar as variáveis de interesse $\hat{x}$ e $\vec{k}$

Como proposta inicial, o problema será abordado de forma alternativa, minimizando um conjunto de variáveis por vez. Caso a convergência não seja assegurada, será pensada outra forma de ataque ao problema.

## Em que conjunto de variáveis aplicar o *denoiser*

Para manter a coerência estrutural do problema, será utilizado o termo de regularização sobre a estimativa da imagem, tornando-se:

\begin{equation*}
	\hat{x}^T (\hat{x} - f(\hat{x}))
\end{equation*}

Em que $f(\cdot)$ representa o *denoiser* utilizado. Demais detalhes sobre a implementação serão discutidos ao longo deste documento.

# Modelagem Matemática

## A Função de custo

O algoritmo de otimização utilizado, dadas entradas ruidosas, torna-se dependente da seguinte função de custo:

\begin{align*}
	\min_{x, k_i} \mathcal{L} (x, \vec{k}, \vec{y}) &= \overbrace{\sum_{j=1}^m ||y_j * k_i - y_i * k_j||^2_2}^{\text{multicanal}} + \\
	&+ \underbrace{\frac{\mu}{2} || x * k_i - y_i ||_2^2}_{\text{erro}} + \overbrace{\frac{\lambda}{2} x^T (x - f(x))}^{\text{RED}}
	\label{eq:lag} 
\end{align*}

## O gradiente de $\mathcal{L}$

Observando as duas variáveis de interesse $x$ e $\vec{k}$ e considerando $f(\cdot)$ como uma função de denoiser localmente homogênea e fortemente passiva, constroem-se os seguintes gradientes:

\begin{equation}
	\nabla_{x} \mathcal{L} = \frac{\mu \partial}{2 \partial x} || x * k_i - y_i ||_2^2 + \lambda (x - f(x))
\end{equation}

## O gradiente de $\mathcal{L}$

\begin{align}
	\nabla_{k_i} \mathcal{L} &= \sum_{j=1}^m \frac{\partial}{\partial k_i} ||y_j * k_i - y_i * k_j||^2_2 + \frac{\mu \partial}{2 \partial k_i} || x * k_i - y_i ||_2^2\\
	\nabla_{k_j} \mathcal{L} &= \sum_{j=1}^m \frac{\partial}{\partial k_j} ||y_j * k_i - y_i * k_j||^2_2 + \frac{\mu \partial}{2 \partial k_j} || x * k_j - y_j ||_2^2
\end{align}

Percebe-se que, quando avalia-se o gradiente de $\mathcal{L}$ sobre os elementos do vetor de kerneis $k_i, k_j \in \vec{k} \ | \ i \neq j$ em que $i$ é um índice de interesse qualquer

## Computando as convoluções no domínio de Fourier

Observando que a operação de convolução pode ser muito custosa - com complexidade de ordem $\mathcal{O}(n^3)$ - utiliza-se o teorema da convolução para reduzir a complexidade total do algoritmo, de forma que

\begin{equation*}
	x * y = \mathcal{F}^{-1} \{ X \cdot Y\} \ | \ X = \mathcal{F} \{ x \}, \ Y = \mathcal{F} \{ y \}
\end{equation*}

# Algoritmo

## Descrição do Método

Para solucionar o problema enunciado, utilizou-se uma abordagem de alternância: hora otimiza-se no subespaço de kerneis, hora otimiza-se no subespaço da imagem restituída. Dessa forma, pode-se dividir o algoritmo de minimização nas seguintes etapas:

\begin{itemize}
	\item \textbf{Passo $\vec{k}$}: otimização de $\mathcal{L}$ sobre o subespaço dos kerneis
	\item \textbf{Passo $x$}: otimização de $\mathcal{L}$ sobre o subespaço da imagem reconstruída
\end{itemize}

## Passo $x$

Encontra $x$ que minimiza $\mathcal{L}$ a partir de algoritmo de minimização por ponto fixo, percebe-se o possível desenvolvimento:

\begin{align}
	\nabla_{x} \mathcal{L} &= 0 \\
	0 &= \frac{\mu \partial}{2 \partial x} || x_k * k_i - y_i ||_2^2 + \lambda (x_k - f(x_{k-1}))
	\label{eq:nabla_x}
\end{align}

## Passo $x$

Dessa forma, isolando $x_k$ dentro da função implícita, encontra-se:

\begin{equation}
x_k = \mathcal{F}^{-1} \Bigg\{ \frac{\lambda \mathcal{F}\{f(x_{k-1})\} + \mu \sum_{i=1}^m K_i^* Y_i}{\lambda + \mu \sum_{i=1}^m K_i^* K_i} \Bigg\}
\end{equation}

Em que a operação $A^* \ | \ A \in \mathbb{C}^{n \times m}$ representa a conjugação complexa dos elementos de uma matriz.

## Passo $\vec{k}$

Solucionar os problemas de minimização relativos aos gradientes $\mathcal{L}_{k_i}$ e $\mathcal{L}_{k_j}$ a partir de uma variação da direção de máxima descida.

# Resultados Preliminares

## Metodologia de teste

Para avaliar primeiramente os desempenhos da regularização e da otimização de $\hat{x}$ propôs-se o seguinte teste:

Otimiza-se o problema por meio do gradiente em (\ref{eq:nabla_x}) utilizando os kerneis de borramento originais. Observa-se o desempenho da otimização por meio da abordagem de ponto-fixo ou pela máxima descida.

<!-- Colocar aqui os diferentes resultados obtidos (gráficos e imagens geradas) -->

## Máxima descida com passo uniforme

![Reconstrução da imagem usando passo uniforme](./figs/recup_fixed_norm.png){ width=150px }

## Máxima descida com passo uniforme

![Lagrangeano por iteração](./figs/lag_fixed_norm.png){ width=250px }

## Máxima descida com passo uniforme

![Gradiente do Lagrangeano por iteração](./figs/grad_fixed_norm.png){ width=250px }

## Máxima descida com passo fixo

![Reconstrução da imagem usando passo uniforme](./figs/recup_fixed.png){ width=150px }

## Máxima descida com passo fixo

![Lagrangeano por iteração](./figs/lag_fixed.png){ width=250px }

## Máxima descida com passo fixo

![Gradiente do Lagrangeano por iteração](./figs/grad_fixed.png){ width=250px }

## Ponto fixo

![Reconstrução da imagem usando passo uniforme](./figs/recup_point.png){ width=150px }

## Ponto fixo

![Lagrangeano por iteração](./figs/lag_point.png){ width=250px }

# Simulações Analíticas

## Descrição do processo

Nesta etapa foram avaliadas alguns comportamentos específicos de determinadas simulações a fim de responder algumas questões:

* Qual o valor ótimo de iterações para cada etapa do algoritmo?
* O número ideal de iterações "globais"
* A relação de dependência entre convergência e kerneis
* A relação de dependência entre convergência e SNR

## Resultados encontrados - 200 iterações a 30 dB

![](./figs_SNR_30dB/k1_simp.png){ width=200px } \
![](./figs_SNR_30dB/k2_simp.png){ width=200px }

\begin{figure}
	\caption{Kerneis encontrados após 200 iterações a 30 dB de SNR}
\end{figure}

## Resultados encontrados - 200 iterações a 30 dB

![imagem encontrada após 200 iterações a 30 db de SNR](./figs_SNR_30dB/recup_simp.png){ width=250px }

## Resultados encontrados - 200 iterações a 30 dB

![Gráficos de RMS por iteração](./figs_SNR_30dB/RMS_simp.png){ width=320px }

## Resultados encontrados - 200 iterações a 30 dB

![Gráfico do custo por iteração](./figs_SNR_30dB/cost_simp.png){ width=320px }

## Resultados encontrados - 200 iterações a 30 dB

![](./figs_SNR_30dB/k1_random.png){ width=200px } \
![](./figs_SNR_30dB/k2_random.png){ width=200px }

\begin{figure}
	\caption{Kerneis encontrados após 200 iterações a 30 dB de SNR}
\end{figure}

## Resultados encontrados - 200 iterações a 30 dB

![imagem encontrada após 200 iterações a 30 db de SNR](./figs_SNR_30dB/recup_random.png){ width=250px }

## Resultados encontrados - 200 iterações a 30 dB

![Gráficos de RMS por iteração](./figs_SNR_30dB/RMS_random.png){ width=320px }

## Resultados encontrados - 200 iterações a 30 dB

![Gráfico do custo por iteração](./figs_SNR_30dB/cost_random.png){ width=320px }

# Perspectivas futuras

## Próximos passos

Para aprofundar a compreensão do trabalho em regimes de baixa SNR alguns próximos passos que podem ser tomados:

* Busca no espaço de hiper-parâmetros ($\lambda$, $\mu$ e $\sigma$);
* Pensar em alternativas para regularização;
* Observar o desempenho dos métodos em outro regime de SNR;
* Verificar o desempenho do RED com máxima descida.
