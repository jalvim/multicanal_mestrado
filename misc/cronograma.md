# Cronograma para próximas atividades

- Até dia 25/02 fechar as correções e testes no código
- Trabalhar no texto do artigo em inglês para sua adaptação às diferentes conferências (todas aceitam artigos de 4 a 6 páginas no padrão i3e, menos a ICSPCS. Essa aceita 10 pp). Prazo para primeira via: 11/03
- Entregar os primeiros dois capítulos da Monografia: 31/03
- Meados de abril (?) Correr atrás do processo de inscrição pro doutorado.

Continuar o trabalho continuado no artigo e na dissertação com o andar do semestre. É importante frisar que diferentes abordagens textuais deverão ser feitas para adequar o artigo para cada espaço e congresso. Dessa forma, haverá trabalho continuado até o dia 05/06, a previsão da última entrega. Nos próximos meses o enfoque será: publicar e terminar a monografia

# Datas importantes (submissão p/ cada conferência)

1. International Conference on Digital Signal Processing (IC-DSP): 11/03
1. International Conference on Advances in Signal Processing (ASPAI): 05/04
1. International Conference on Signal Processing and Communication System (ICSPCS): 14/04
1. International Conference on Frontiers of Signal Processing (ICFSP): 05/06
