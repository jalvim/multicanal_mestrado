---
title: Deconvolução Multicanal para Imagens Ruidosas
subtitle: Dissertaçao de Mestrado
date: 2023
author:
	- João Rabello Alvim
	- Orientador - Prof. Dr. Renato da Rocha Lopes
	- Coorientador - Prof. Dr. Kenji Nose-Filho
theme: AnnArbor
colortheme: seahorse
affiliation: DECOM - FEEC - Unicamp
endnote: true
toc: true
toc-depth: 5
---

# Introdução

## Problemas Inversos

Considerando uma dada imagem $y$, um canal $k$ e um ruído estocástico $n$, percebe-se:

\phantom{uuuui}

![Fluxograma de interação sinal $\times$ canal](./assets/inverse_prob.png){ width=350px }

<!-- \begin{align}
	\begin{split}
		y(i, j) &= (x * k) (i, j) + n(i, j) \\
		&= \sum_l \sum_k x(l, k) k(i-l, j-k) + n(i, j)
	\end{split}
	\label{eq:inv}
\end{align} -->

\phantom{uuuui}

Encontrar $y$ dado uma imagem de interesse $x$, a ação linear do canal $k$ e ruído $n$ é o processo chamado de **problema direto**.

## Problemas Inversos

- Encontrar $x$ a partir da aquisição $y$ é um **problema inverso**.

- O problema de deconvolução pode ser extremamente **mal-posto**.

![Fonte: Jaejun Yoo, 2019. Image Restoration (IR): inverse problem point of view (1). acessado em 05/02/2024 pelo [link](https://jaejunyoo.blogspot.com/2019/05/image-restoration-inverse-problem-1.html)](./assets/inverse-probleme_325.png){ width=300px }

## Problemas Inversos

- Para solucionar este problema, lançamos mão de **técnicas de regularização**.

![Takehiko, Ogawa & Hajime, Kanada. (2010). Solution for Ill-Posed Inverse Kinematics of Robot Arm by Network Inversion.](./assets/ill-posedness.png){ width=200px }

<!-- ## Problemas Inversos

![Representação gráfica do fluxo direto de um problema](./assets/inverse_prob.png){ width=350px } -->

<!-- ## Multiple Channels

In some applications, the same image can be captured by multiple sensors

$$
	\begin{cases}
		y_1 = k_1 * x \\
		\quad\quad\vdots \\
		y_i = k_i * x \\
		\quad\quad\vdots \\
		y_m = k_m * x \\
	\end{cases}
$$

This model is known by SIMO (*Single Input Multiple Output*) -->


<!-- The proposed approach consists in combining MBD with the Regularization by Denoising (RED) technique -->

<!-- ## Motivation

- Any captured image will be distorted by an usually unknown channel (or kernel)

- In order to retrieve the original signal, some deconvolution technique should be applied over the measurement

- This is an **inverse problem** -->

<!-- PENSAR EM MAIS COISAS PRA FALAR NA MOTIVAÇÃO! -->

<!-- ## Inverse Problems

### Ill-posedness

Following Haddamard definition, a well posed problem needs to satisfy the following conditions:

\phantom{uuuuui}

1. There must **exist** a solution to the problem

1. This solution must be **unique**

1. The solution must depend **continuously** to the input

\phantom{uuuuui}

If one of this conditions isn't satisfied, the problem is then **ill-posed** \cite{HadamardSurLP} -->

## Múltiplos Canais

![Neste trabalho estuda-se a deconvolução por **critério multicanal**.](./assets/SIMO_model.png){ width=250px }

## Motivação

A Deconvolução multicanal (MBD) pode ser usada em diversos campos:

## Motivação

### Imageamento Microscópico

![Retirada de [\textcolor{blue}{Education in Microscopy and Digital Imaging}.](https://zeiss-campus.magnet.fsu.edu/articles/spectralimaging/introduction.html) ](./assets/microscopy.jpg){ width=250px }

## Motivação

### Imageamento satelital

![ Retirada de *Pagliarini, Giovanni & Sciavicco, Guido. (2021). Decision Tree Learning with Spatial Modal Logics.*](./assets/satellite.png){ width=250px }

## Motivação

### Sensores em arranjos (câmeras em sistemas *mobile*)

![Retirada de *Eric E Thomson, Mark Harfouche, Kanghyun Kim, Pavan C Konda, Catherine W Seitz, Colin Cooke, Shiqi Xu, Whitney S Jacobs, Robin Blazing, Yang Chen, Sunanda Sharma, Timothy W Dunn, Jaehee Park, Roarke W Horstmeyer, Eva A Naumann (2022) Gigapixel imaging with a novel multi-camera array microscope
eLife 11:e74988.*](./assets/camera_array.png){ width=250px }

## Motivação

### Estado da arte

- Regularização por meio de **MGD**, do inglês: *manifold gradient descent* (\textcolor{blue}{SHI; CHI,
2021}) - Imagens sem ruído.

\phantom{uuuui}

![Vizualização de região de busca para o *MGD* - Retirado da referência.](./assets/shi-chi.png){ width=350px }

## Motivação

### Estado da arte

- MBD para sinais *esparsos* e suas condições de identificabilidade (\textcolor{blue}{MULLETI; LEE; ELDAR, 2020}).

- Melhora de S-MBD para imagens com *Unfolding Neural Networks* (\textcolor{blue}{TOLOOSHAMS et al., 2021}).

\phantom{uuuui}

![Arquitetura de *Deep Unfolding* the Tolooshams *et al.*](./assets/tolooshams.png){ width=350px }

## Motivação

### Estado da arte

- Romano *et al.* introduz a **Regularização por *denoising* ** (RED) (\textcolor{blue}{ROMANO; ELAD; MILANFAR, 2016}).

## Motivação 

### Nossa contribuição

Encontrar um método que seja:

- Independente do *dataset*;
- Independente da geometria dos *kernels*;
- Robusto à ruído (AWGN).

## Motivação

- MBD pode recuperar sinais sem informação prévia sobre o canal, mas é extremamente \textcolor{red}{mal-posta}.

- RED é uma **regularização**, mas depende de \textcolor{red}{conhecimento prévio} dos *kernels* de borramento.

- **Por que não combinar ambos?**

<!-- ## Convolution Through Multiple Channels

Given two channels $k_i, k_j \in \{k_1, \dots, k_m\}$ and a reference signal $x$. One can build the following system:
\begin{align*}
	y_i &= k_i * x \\
	y_j &= k_j * x
\end{align*} -->

<!-- # Mathematical Model -->

<!-- ## Convolution Through Multiple Channels

Knowing that the convolution operation is **commutative**, one can notice that:

\begin{align*}
	y_i * k_j &= (k_i * x) * k_j \\
	&= k_i * (x * k_j) \\
	&= k_i * y_j \\
	&= y_j * k_i \\
\end{align*}

Which implies: $y_i * k_j - y_j * k_i = 0$ -->

# Modelagem Matemática

## Notação

### Neste trabalho convenciona-se:

- \textcolor{blue}{Sinais}: Letras minúsculas (normalmente **matriciais**).
$$p[x, y] \equiv p$$
- \textcolor{blue}{T.F.}: Letras maísculas (normalmente **matriciais**).
$$X \equiv \mathcal{F}\{x\}$$
- \textcolor{blue}{Arranjos}: Indexados por subíndices.
$$X_i \in \vec{X} \implies \vec{X} \equiv \{X_1 \dots X_i \dots X_n\}$$

## Convolução por múltiplos canais

Sabendo que a convolução é \textcolor{blue}{comutativa} o que implica: 

\begin{align*}
\mathcolor{blue}{y_i * k_j} &= (x * k_i) * k_j \\
	  &= k_i * (x * k_j) \\
	  &= k_i * y_j \\
	  &= \mathcolor{blue}{y_j * k_i} \\
\end{align*}

## Convolução por múltiplos canais

### Função de custo

\begin{equation}
	\min_{x, k_i} \mathcal{L} (\vec{k}, \vec{y}) = \sum_{j \neq i} ||y_j * k_i - y_i * k_j||^2_2
	\label{eq:lag} 
\end{equation}

<!-- A condição de identificabilidade deste problema é assegurada quando $k_i$ e $k_j$ não compartilham zeros em comum, ou seja, ambas as transformadas $Z$ do sistema não possuem ambiguidade em seus polinômios. Visto que qualquer combinação linear de $k_i$ e $k_j$ também compartilhará o mesmo zero. -->

## Modelagem Matemática

No entanto, percebe-se que a partir da seguinte matriz...

\begin{equation*}
	\mathbb{Y} = 
	\begin{bmatrix}
		C_{y_2} & C_{y_3} & \dots & C_{y_m} &  & & & & \\
		-C_{y_1} & & & &  C_{y_3} & \dots & C_{y_m} & & \\
		& -C_{y_1} & & & -C_{y_2} & & & \dots & \\
		& & \ddots & & & \ddots & & & C_{y_m} \\
		& & & -C_{y_1} & & & -C_{y_2} & & -C_{y_{m-1}}
	\end{bmatrix}^T
	\label{eq:mat}
\end{equation*}

## Modelagem Matemática

... resolver (\ref{eq:lag}) torna-se equivalente à seguinte formulação de quadrados mínimos:

\begin{align}
	\begin{split}
		\min &\quad \vec{k}^T \mathbb{Y}^T \mathbb{Y} \vec{k} \\
		\text{s.t.} &\quad ||\vec{k}||_2 = 1
	\end{split}
	\label{eq:quad_prob}
\end{align}

No entanto, resolver (\ref{eq:quad_prob}) é uma tarefa computacionalmente \textcolor{red}{complexa}.

## Modelagem Matemática

- Para reduzir a complexidade, utilizamos o **teorema da convolução** e realizamos as operações no domínio das frequências.

- Para melhor posicionar: regularizamos a função de custo.

## Ruído e Regularização

Outra métrica de controle importante é o \textcolor{blue}{erro de reconstrução}, que pode ser acrescido à eq. (\ref{eq:lag}):

\phantom{uuuui}

### Regularização por Erro de Reconstrução

$$
	\frac{\mu}{2} ||x * k_i - y_i||_2^2
$$

## Ruído e Regularização

RED é baseado no pressuposto de que o subespaço de ruído e de sinal são **ortogonais**.

\phantom{uuuui}

### Regularização por *denoising* (RED)

\begin{align*}
	&\lambda x^T\big(x - f(x)\big) \\
	&\frac{\partial}{\partial x} \big(x^T(x - f(x))\big) \approx x - f(x)
\end{align*}

<!-- ## Noise and Regularization

In order to a denoiser $f(\cdot)$ be able to perform RED, it must satisfy two conditions:

\phantom{uuuui}

1. Local Homogeneity: $f(x) = \nabla_x f(x) \cdot x$

1. (Strong) Passivity: $\rho(f) \leq 1 \implies \rho(\nabla_x f) ||x|| \leq ||x||$, where $\rho(f)$ is the spectrum range of the denoiser

<!-- ## Noise and Regularization

The RED algorithm can be evaluated in three distinct ways

\phantom{uuuui}

1. Steepest Descent

1. Fixed Point

1. ADMM -->

# Metodologia

## O Problema trabalhado

Para os testes com dados sintéticos foram usadas as seguintes imagens:

![Imagem de referência (original).](./figs/starfish_pb.png){ width=125px }

## O Problema trabalhado

![Imagem borrada pelo *kernel* aleatório 1.](./figs/kernel_rand1.png){ width=350px }

## O Problema trabalhado

![Imagem borrada pelo *kernel* aleatório 2.](./figs/kernel_rand2.png){ width=350px }

## O Problema trabalhado

![Imagem borrada pelo *kernel* ``simplificado'' 1.](./figs/kernel_simp1.png){ width=350px }

## O Problema trabalhado

![Imagem borrada pelo *kernel* ``simplificado'' 2.](./figs/kernel_simp2.png){ width=350px }

## Cenários Trabalhados

### Organização do trabalho

1. Cenário A: Dados sintéticos sem AWGN;
1. Cenário B: Dados sintéticos com AWGN;
1. Cenário C: Dados Reais.

# Cenário A - Descrição e Resultados

## Implementação Proposta

### Configurações da otimização

* Máxima descida sobre eq. (\ref{eq:lag});

* Implementação na linguagem Python;

* 100 mil iterações no algoritmo;

* Comparações entre as buscas lineares:

	1. Seção Áurea;

	1. Busca Global;

	1. Barzilai-Browein;

	1. Passo fixo.

## Implementação Proposta

A reconstrução da imagem é obtida com a equação[^1]:

$$
	\hat{x} = \mathcal{F}^{-1}\Bigg\{ \frac{Y_i}{K_i} \Bigg\}
$$

[^1]: Para cada estimativa de *kernel*, há uma reconstrução de imagem.

<!-- ## Gradient Descent

- In order to find the solutions $\hat{x}$ and $\hat{k_i}$, we must perform **gradient descent**
- The solutions are found iteratively by choosing the **steepest descent direction** ($\nabla_x \mathcal{L}$ or $\nabla_{k_i} \mathcal{L}$)

\begin{align*}
	x^{l} = x^{l-1} - \sigma \nabla_x \mathcal{L} \\
	k_i^{l} = k_i^{l-1} - \sigma \nabla_k{_i} \mathcal{L}
\end{align*}

\phantom{uuuui}

In which $\sigma \in \mathbb{R}$ is a constant calculated by a given line search technique and $\{x^l\}_{l=1}^\infty \rightarrow \hat{x}$ as well as $\{k_i^l\}_{l=1}^\infty \rightarrow \hat{k_i}$ -->

## Gradiente Descendente

- Calcula-se a direção de máxima descida por meio do **gradiente da função de custo**.
- As operação foram calculadas no **domínio das frequências** $\implies$ \textcolor{blue}{baixa complexidade}.

\begin{equation*}
	\nabla_{k_i} \mathcal{L} = 2 \sum_{i \neq j} \mathcal{F}^{-1}\{Y_j^* \cdot (K_i \cdot Y_j - K_j \cdot Y_i) \}
	%&+ \mu \sum_{i=1}^m \mathcal{F}^{-1} \big\{X^* \cdot (X \cdot K_i - Y_i) \big\} \\
	%\nabla_{k_j} \mathcal{L} &= 2 \sum_{i \neq j} \mathcal{F}^{-1}\{-Y_i^* \cdot (K_i \cdot Y_j - K_j \cdot Y_i)\} \\
\end{equation*}

<!-- \phantom{uuuui}

Where $\mathcal{F} \{ \cdot \}$ represents the **Fourier transform** of a given signal. -->

## Método da Seção Áurea

- Boa alternativa para funções de custo **unimodais**.[^2]

![Exemplo de busca de passo exata](./assets/unimodal_golden_section.png){ width=200px }

[^2]: Figuras retiradas de: *Otimização contínua, aspectos teóricos e computacionais - Ademir Ribeiro e Elizabeth Karas, 2013*.

## Método da Seção Áurea

- Avaliação de $\mathcal{L}$ sobre a tripartição $\{0, \theta, 1-\theta, 1\}$ com $\theta = \frac{3 - \sqrt{5}}{2}$.
- Minimiza acessos a $\mathcal{L}$ $\implies$ \textcolor{blue}{um único acesso por iteração}.

\centering

![](./assets/particao_golden_section.png){ width=150px }
![](./assets/particao2_golden_section.png){ width=150px }

Exemplos de tri-partição e atualização de intervalo.

## Busca Global

- Neste método particiona-se um intervalo $[a, b]$ em $\delta$ segmentos.

- O passo $\sigma$ é dado por $j\frac{b-a}{\delta}$ where $j \in [0, 1, \dots, \delta - 1]$.

- O índice ótimo $j$ é aquele que minimiza $\mathcal{L}$.

### \textcolor{red}{Contras}

A função de custo é avaliada $\delta$ vezes.

## Método Barzilai-Browein

- É um método Quase-Newton que calcula uma \textcolor{blue}{aproximação de primeira ordem} da matriz Hessiana

\begin{align}
	\sigma^\prime &= \frac{<\mathrm{vec}(\Delta k), \mathrm{vec}(\Delta g)>}{<\mathrm{vec}(\Delta g), \mathrm{vec}(\Delta g)>} \\
	\sigma^{\prime\prime} &= \frac{<\mathrm{vec}(\Delta k), \mathrm{vec}(\Delta k)>}{<\mathrm{vec}(\Delta k), \mathrm{vec}(\Delta g)>}
	\label{eq:dir_b_2}
\end{align}

-   Este método gera dois passos candidatos $\sigma^\prime$ e $\sigma^{\prime \prime}$, ambos viáveis.

### \textcolor{green}{Pros}

Não avalia $\mathcal{L}$, apenas $\nabla_k \mathcal{L}(k^l)$, $\nabla_k \mathcal{L}(k^{l-1})$ e seus valores passados.

## Método Barzilai-Browein

- As imagens $\Delta x$ e $\Delta g$ são calculadas pelas expressões:

\begin{align*}
	\Delta k &= k^l - k^{l-1} \\
	\Delta g &= \nabla_k \mathcal{L}(k^l) - \nabla_k \mathcal{L}(k^{l-1})
\end{align*}

## Passo fixo

É utilizado um escalar $\sigma \in \mathbb{R}$ ponderado por uma constante específica (para os propósitos deste trabalho foi utilizado o **inverso do módulo do gradiente**).

## Resultados

\centering

![Erro quadrático médio entre reconstruções e imagem de referência para cada método.](./figs/rmse.png){ width=250px }

## Resultados

\centering

![Estimativa para *kernel* 1 (seção áurea).](./figs/k1_random_secau.png){ width=250px }

## Resultados

\centering

![Estimativa para *kernel* 2 (seção áurea).](./figs/k2_random_secau.png){ width=250px }

## Resultados

\centering

![Estimativa para *kernel* 1 (passo fixo).](./figs/k1_bicanal.png){ width=250px }

## Resultados

\centering

![Estimativa para *kernel* 2 (passo fixo).](./figs/k2_bicanal.png){ width=250px }

## Resultados

\centering

![Estimativa para *kernel* 1 (busca global).](./figs/ker_bb_a.png){ width=250px }

## Resultados

\centering

![Estimativa para *kernel* 2 (busca global).](./figs/ker_bb_b.png){ width=250px }

## Resultados

\centering

![Estimativa para *kernel* 1 (Barzilai-Browein).](./figs/k1_bicanal_bb.png){ width=250px }

## Resultados

\centering

![Estimativa para *kernel* 2 (Barzilai-Browein).](./figs/k2_bicanal_bb.png){ width=250px }

# Cenário B - Descrição e Resultados

## Função de Custo 

Para uma modelagem do problema mais regularizada, utilizou-se a seguinte função de custo:

\begin{align*}
	\min_{x, k_i} \mathcal{L} (x, \vec{k}, \vec{y}) &= \overbrace{\sum_{j=1}^m ||y_j * k_i - y_i * k_j||^2_2}^{\text{Critério Multicanal}} + \\
	&+ \underbrace{\frac{\mu}{2} || x * k_i - y_i ||_2^2}_{\text{Erro de Reconstrução}} + \underbrace{\lambda x^T (x - f(x))}_{\text{RED}}
	\label{eq:lag_aug} 
\end{align*}

## RED de ponto fixo (Passo-$x$)

Detectando o **ponto fixo** (onde $\nabla_x \mathcal{L} = 0$), as condições do RED e assumindo que $x^l \perp \big(x^{l-1} - f(x^{l-1})\big)$, pode-se estabelecer a seguinte fórmula iterativa para $x^ l$:

\begin{equation}
x^l = \mathcal{F}^{-1} \Bigg\{ \frac{\lambda \mathcal{F}\{f(x^{l-1})\} + 2 \mu \sum_{i=1}^m K_i^* Y_i}{\lambda + 2 \mu \sum_{i=1}^m K_i^* K_i} \Bigg\}
\end{equation}

## Implementação Proposta

### Dados da otimização

* Minimização alternada de $\mathcal{L}$ para domínios $x$ e $k_i$;

* $\lambda = 0.5$ e $\mu = 1.0$;

* Usamos o **RED de ponto fixo** para o passo-$x$ com *denoiser* NLM (*Non-Local Means*);

* 20 macro-iterações e 500 micro-iterações (cada domínio):
	- $100$ mil iterações no total.

* Imagens com 30 dB de ruído sintético.

## Resultados

\centering

![MSE das estimativas para os *kernels* simplificados.](./figs/RMS_simp.png){ width=300px }

## Resultados

\centering

![Função de custo das estimativas para os *kernels* simplificados.](./figs/Cost_simp.png){ width=300px }

## Resultados

\centering

![Reconstruções obtidas para *kernels* simplificados.](./figs/ker_simp.png){ width=300px }

## Resultados

\centering

![MSE das estimativas para os *kernels* aleatórios.](./figs/RMS_rand.png){ width=300px }

## Resultados

\centering

![Função de custo das estimativas para os *kernels* aleatórios.](./figs/Cost_rand.png){ width=300px }

## Resultados

\centering

![Reconstruções obtidas para *kernels* aleatórios.](./figs/ker_rand.png){ width=300px }

# Cenário C - Descrição e Resultados

## Implementação Proposta

### Dados da otimização

- Comparação entre RED clássico (com informação sobre os *kernels*):
	- 150 iterações para o RED clássico;
	- 20 macro-iterações e 500 micro-iterações (cada domínio).

- $\lambda = 0.5$ e $\mu = 1.0$;

- Usamos o **RED máxima descida** para o passo-$x$ e denoiser **Fast non-local Means**;

- 20 macro-iterações e 500 micro-iterações (cada domínio):
	- $100$ mil iterações no total.

- Imagens obtidas por Sroubek *et al.*.

## Implementação Proposta

### Métricas de Avaliação

- PSNR: Avalia (em dB) a diferença média por pixel entre duas imagens.
- RMSE: Avalia a raiz quadrada do erro quadrático médio (\textcolor{blue}{raiz quadrada da energia do erro}).
- SSIM: Avalia a *similaridade estrutural* entre duas imagens.

## Entradas da Base

![Entradas e saída obtidos pelo [link](http://zoi.utia.cas.cz/deconvolution.html).](./figs/input_real.png){ width=350px }

## Imagem de Referência

![Imagem disponibilizada junto ao *dataset* em tons de cinza.](./figs/output-modified.png){ width=250px }

## Entradas da base

*Kernels* de borramento determinados por Sroubek:

\phantom{uuuuui}
\centering
![](../../data_set3/psf1.png){ width=100px }
![](../../data_set3/psf2.png){ width=100px }

## Entradas da base

*Priors* usados como ponto inicial da busca no Passo-$k_i$ para o método alternado:

\phantom{uuuuui}
\centering
![](../../data_set3/prior_1.png){ width=100px }
![](../../data_set3/prior_2.png){ width=100px }

## Resultados com RED clássico

- RMSE = $0,589$, PSNR = $51,82$ dB e SSIM = $93,368$ %

\centering

![](./figs/output_real_red.png){ width=200px }
![](./figs/output-modified.png){ width=200px }

Reconstrução obtida pelo RED supervisionado comparada à referência.

## Resultados com método proposto

- RMSE = $0,283$, PSNR = $59,11$ dB e SSIM = $94,965$ %

\centering

![](./figs/output_real_our.png){ width=200px }
![](./figs/output-modified.png){ width=200px }

Reconstrução obtida pelo método iterativo comparada à referência.

## Resultados com método proposto

No entanto, para a reconstrução dos kernels obteve-se...

![Recuperação dos *kernels* ao fim do método alternado](./figs/recup_ker_our.png){ width=250px }

# Conclusões

## Conclusões

- A busca linear de passo é **crucial** para a deconvolução;

- Buscas mais simples tem desempenho comparável a buscas mais complexas;

- Boas estimativas para $x$, mas nem tanto para $k_i$...
	- Regularizar $k_i$? Aplicações específicas.
	- Boas inicializações para $k_i$.

## Conclusões

- Para dados ruidosos, alguma regularização é **necessária**:
	- O método RED é muito poderoso para este fim!
	- A regularização em $x$ auxilia a otimização em $k_i$.

\phantom{uuuui}

### Perspectivas futuras

- Acertar valor ótimo das constantes $\lambda$ e $\mu$ para cenários de baixa SNR;

- Verificar possíveis regularizações para $k_i$.

##

\centering
\Large \textbf{Obrigado pela atenção!!!}

## Referências Bibliográficas

\centering

![](./assets/bib1.png){ width=250px }

## Referências Bibliográficas

\centering

![](./assets/bib2.png){ width=250px }

## Referências Bibliográficas

\centering

![](./assets/bib3.png){ width=250px }
