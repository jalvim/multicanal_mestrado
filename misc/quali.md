---
title: Desconvolução Multicanal para *Deblurring* de Imagens Ruidosas
subtitle: Apresentação de artigo - SBrT
author: João Rabello Alvim
theme: Madrid
bibliography: bibliografia.bib
endnote: true
toc: true
toc-depth: 3
---

# Introdução

## Problemas inversos

Considerando uma imagem percebida $y$, um canal $k$ e ruído estatístico $n$, modela-se:

\begin{align}
	\begin{split}
		y(i, j) &= (x * k) (i, j) + n(i, j) \\
		&= \int_{-\infty}^\infty \int_{-\infty}^\infty x(\alpha, \beta) k(i - \alpha, j - \beta) d\alpha d\beta + n(i, j)
	\end{split}
	\label{eq:inv}
\end{align}

Encontrar o sinal $y$ a partir de $k, \ x \ \text{e} \ n$ é solucionar o problema em sua formulação **direta**.

## Múltiplos canais

Em alguns meios, conta-se com diversidade de canais

$$
	\begin{cases}
		y_1 = k_1 * x \\
		\vdots \\
		y_i = k_i * x \\
		\vdots \\
		y_m = k_m * x \\
	\end{cases}
$$

Essa abordagem do problema é conhecida como SIMO (*Single Input Multiple Output*)

## Problemas inversos

* Encontrar $x$ a partir de $y$ em (\ref{eq:inv}) é um **problema inverso**

* Um problema inverso de deconvolução pode ser extremamente mal posto, ou seja, apresentar **múltiplas soluções**.

* Para resolver este problema: **Técnicas de regularização**

* Será usada a técnica de **deconvolução multicanal**

## Motivação

* Quando capturamos imagens ou qualquer tipo de sinal estamos expostos ao efeito do canal sobre o sinal de interesse

* Para recuperar a informação original é necessário estimar o canal e deconvoluir seu efeito do sinal adquirido

* Este é um tipo de problema inverso

## Motivação

* Diversos canais físicos podem ser interpretados como múltiplos canais
	* Imagens microscópicas
	* Imagens satelitais
	* Múltiplos sensores

## Motivação

Em determinadas aplicações, o canal pode ter efeito bastante significativo sobre um sinal

![Imagem original](./figs/starfish_pb.png){ width=125px }

## Motivação

![Imagem borrada com representação gráfica de canal (*kernel* aleatório)](./figs/kernel_rand1.png){ width=350px }

## Motivação

![Imagem borrada com representação gráfica de canal (*kernel* aleatório)](./figs/kernel_rand2.png){ width=350px }

## Motivação

![Imagem borrada com representação gráfica de canal (*kernel* simplificado)](./figs/kernel_simp1.png){ width=350px }

## Motivação

![Imagem borrada com representação gráfica de canal (*kernel* simplificado)](./figs/kernel_simp2.png){ width=350px }

# Revisão da Literatura e Modelagem Matemática

## Estado da arte

* A técnica de deconvolução multicanal é bastante utilizada em diversas aplicações, tais como análise de sísmica de sinais \cite{nose-filho_improving_2018}, *deblurring* de imagens \cite{cho_fast_2009} e hiper resolução em imagens satelitais \cite{lee_supersampling_2002}

* Para problemas inversos que involvam imagens ruidosas, Romano et al. propõe a regularização RED (*REgularization by Denoising*) \cite{romano_little_2017}

* Para recuperação de imagens borradas por canais esparsos, Shi et al. propõe o próprio algoritmo de descida de gradiente enquanto regularização para a minimização \cite{shi_manifold_2021}

## A convolução por diversos canais

Supõe-se a existência de dois canais $k_i, k_j \in \{k_1, \dots, k_m\}$ juntamente com um determinado sinal $x$. Sabe-se que a saída de cada sistema é dada por:

\begin{align*}
	y_i &= k_i * x \\
	y_j &= k_j * x
\end{align*}

## Modelagem Matemática

Sabendo que a operação de convolução é **comutativa**, percebe-se:

\begin{align*}
	y_i * k_j &= (k_i * x) * k_j \\
	&= k_i * (x * k_j) \\
	&= k_i * y_j \\
	&= y_j * k_i \\
\end{align*}

O que implica: $y_i * k_j - y_j * k_i = 0$

## Modelagem Matemática

Aplicando a propriedade desenvolvida acima para as $m$ capturas disponíveis, pode-se obter a deconvolução a partir da minimização de uma função de custo.

\phantom{uuuuui}

### Função de custo

\begin{equation}
	\min_{x, k_i} \mathcal{L} (x, \vec{k}, \vec{y}) = \sum_{\substack{j=1 \\ j \neq i}}^m ||y_j * k_i - y_i * k_j||^2_2
	\label{eq:lag} 
\end{equation}

<!-- A condição de identificabilidade deste problema é assegurada quando $k_i$ e $k_j$ não compartilham zeros em comum, ou seja, ambas as transformadas $Z$ do sistema não possuem ambiguidade em seus polinômios. Visto que qualquer combinação linear de $k_i$ e $k_j$ também compartilhará o mesmo zero. -->

## Modelagem Matemática

No entanto, percebe-se que a função de custo é construída a partir das diferenças das diferentes convoluções entre *kernels*. Dessa forma, constrói-se a matriz:

\begin{equation*}
	\mathbb{Y} = 
	\begin{bmatrix}
		C_{y_2} & C_{y_3} & \dots & C_{y_m} &  & & & & \\
		-C_{y_1} & & & &  C_{y_3} & \dots & C_{y_m} & & \\
		& -C_{y_1} & & & -C_{y_2} & & & \dots & \\
		& & \ddots & & & \ddots & & & C_{y_m} \\
		& & & -C_{y_1} & & & -C_{y_2} & & -C_{y_{m-1}}
	\end{bmatrix}^T
	\label{eq:mat}
\end{equation*}

## Modelagem Matemática

Dessa forma, percebe-se que a resolução da equação (\ref{eq:lag}) é equivalente à minimização da seguinte forma quadrática:

\begin{align}
	\begin{split}
		\min &\quad \vec{k}^T \mathbb{Y}^T \mathbb{Y} \vec{k} \\
		\text{s.a.} &\quad ||\vec{k}||_2 = 1
	\end{split}
	\label{eq:quad_prob}
\end{align}

No entanto a resolução de (\ref{eq:quad_prob}) é demasiada custosa computacionalmente.

## Modelagem Matemática

* Para solucionar o problema associado à complexidade, calculam-se as operações de convolução no **domínio das frequências**

* Para tratar do enorme espaço de busca das soluções foram propostas duas **regularizações**

## Ruído e Regularizações

No entanto, não queremos apenas os canais, quer-se identificar também a reconstrução do sinal. Dessa forma, acrescenta-se à (\ref{eq:lag}):

\phantom{uuuui}

### Regularização via Erro de Reconstrução

* Minimiza a energia do erro de reconstrução

$$
	\frac{\mu}{2} ||x * k_i - y_i||_2^2
$$


## Ruído e Regularizações

A regularização RED (*REgularization by Denoising*), que incentiva soluções ortogonais à estimativa de ruído. A função $f(\cdot)$ representa um **denoiser**

\phantom{uuuui}

### Regularização RED

* Privilegia soluções ortogonais ao ruído

$$
	\frac{\lambda}{2} x^T\big(x - f(x)\big)
$$

## Ruído e Regularizações

Para o *denoiser* $f(\cdot)$ funcionar de maneira adequada. Para isso, foi escolhido o denoiser NLM. Romano et al. \cite{romano_little_2017} propõem que este apresente duas propriedades:

\phantom{uuuui}

1. Homogeinedade local: $f(x) = \nabla_x f(x) \cdot x$
1. Passividade forte: $\rho(f) \leq 1 \implies \rho(\nabla_x f) ||x|| \leq ||x||$

## Função de Custo Aumentada

Para superar o quão mal-posto este problema pode ser, a função de custo otimizada na aplicação torna-se:

\begin{align*}
	\min_{x, k_i} \mathcal{L} (x, \vec{k}, \vec{y}) &= \overbrace{\sum_{j=1}^m ||y_j * k_i - y_i * k_j||^2_2}^{\text{multicanal}} + \\
	&+ \underbrace{\frac{\mu}{2} || x * k_i - y_i ||_2^2}_{\text{erro}} + \overbrace{\frac{\lambda}{2} x^T (x - f(x))}^{\text{RED}}
	\label{eq:lag_aug} 
\end{align*}

# Metodologia

## Implementação Proposta

### Otimização Simplificada

* Utilização de algoritmo de gradiente descendentes sobre (\ref{eq:lag})

* Implementação em Python

* 100E$^3$ iterações totais

* Comparação entre diferentes algoritmos de busca de passo:

	1. Seção Áurea
	1. Busca Global
	1. Barzilai-Browein
	1. Passo Fixo

## Implementação Proposta

### Otimização com Regularização

* Descida de gradiente alternada sobre $x$ e $k$

* Utiliza **regularizações** em $x$, portanto, otimiza-se (\ref{eq:lag_aug})

* Utilização de RED de ponto fixo (caso ruidoso)

* 100E$^3$ iterações totais (par de descidas alternadas)

## Gradiente Descendente

Busca-se encontrar as soluções ótimas $x$, $k_i$ e $k_j$ de $\mathcal{L}$ a partir do processo iterativo de **gradiente descendente**.

\begin{align*}
	k_i^\nu &= k_i^{\nu-1} - t \nabla_{k_i} \mathcal{L} \\
	%k_j^\nu &= k_j^{\nu-1} - t \nabla_{k_j} \mathcal{L} \\
	x^\nu &= x^{\nu-1} - t \nabla_{x} \mathcal{L}
\end{align*}

<!-- O operador $\nabla_f \mathcal{L}$ representa o gradiente da função de custo sobre o sinal $f$ (tipicamente $x$ ou $k$) e o parâmetro $t$, o comprimento do passo (melhor discutido nos próximos slides). -->

## Gradiente Descendente

Para a execução do algoritmo, foram calculados os seguintes gradientes (todos utilizados durante a otimização)

\begin{align*}
	\nabla_{k_i} \mathcal{L} &= 2 \sum_{\substack{Y_j \in \vec{Y} \\ i \neq j}} \mathcal{F}^{-1}\{Y_j^* \cdot (K_i \cdot Y_j - K_j \cdot Y_i) \} \\
	%\nabla_{k_j} \mathcal{L} &= 2 \sum_{\substack{Y_i \in \vec{Y} \\ i \neq j}} \mathcal{F}^{-1}\{-Y_i^* \cdot (K_i \cdot Y_j - K_j \cdot Y_i)\} \\
	\nabla_{x} \mathcal{L}   &= \mu \mathcal{F}^{-1}\{K_i^* \cdot (X \cdot K_i - Y_i)\} + \lambda (x^k - f(x^{k-1}))
\end{align*}

## Método da Seção Áurea

* Boa alternativa para para funções de custo **unimodais**

* Determinação de intervalo inicial

* Avaliação de $\mathcal{L}$ sobre a tripartição $\{0, \theta, 1-\theta, 1\}$ com $\theta = \frac{3 - \sqrt{5}}{2}$

* Forma eficiente de minimizar acessos a $\mathcal{L}$ (um único acesso por iteração)

* Mais detalhes podem ser consultados na subrotina 6.4 de \cite{ribeiro_e_karas_2013}

## Método da Busca Global

A busca global consiste na partição de um intervalo $[a, b]$ em $\sigma$ partições, logo, $t = j\frac{b-a}{\sigma}$ em que $j \in [0, 1, \dots, \sigma - 1]$. O índice $j$ é, portanto, escolhido de forma a minimizar $\mathcal{L}$ sobre a partição de interesse. Acessa $\mathcal{L}$ $\sigma$ vezes

## Método Barzilai-Browein

Este método é uma aproximação de primeira ordem para a direção dada pelo passo de Newton (Jacobiano ponderado pela matriz Hessiana)

\begin{align*}
	\Delta k &= k^\nu - k^{\nu-1} \\
	\Delta g &= \nabla_k \mathcal{L}(k^\nu) - \nabla_k \mathcal{L}(k^{\nu-1})
\end{align*}

Não acessa $\mathcal{L}$, sim os gradientes $\nabla_k \mathcal{L}(k^\nu)$ e $\nabla_k \mathcal{L}(k^{\nu-1})$ e valores passado e atual e $k_i$. Todos já previamente calculados.


## Método Barzilai-Browein

A partir das diferenças acima, encontram-se as seguintes aproximações da direção de Newton:

\begin{align}
	t^\prime &= \frac{<\Delta k, \Delta g>}{<\Delta g, \Delta g>} \\
	t^{\prime\prime} &= \frac{<\Delta k, \Delta k>}{<\Delta k, \Delta g>}
\end{align}

## Método do Passo Fixo

Utiliza-se um passo $t$ de comprimento fixo (tipicamente $10^{-5}$) e utiliza-se este comprimento em **todas** as iterações do gradiente descendente

## RED de Ponto Fixo

Segundo Romano et al. em \cite{romano_little_2017}, uma das vantagens de sua regularização é a convergência de um **ponto fixo**, ou seja, a possibilidade de uma forma iterativa sem a necessidade de cálculo de comprimento de passo.

\begin{equation}
x_\nu = \mathcal{F}^{-1} \Bigg\{ \frac{\lambda \mathcal{F}\{f(x^{\nu-1})\} + 2 \mu \sum_{i=1}^m K_i^* Y_i}{\lambda + 2 \mu \sum_{i=1}^m K_i^* K_i} \Bigg\}
\end{equation}

# Resultados Preliminares

## Resultados (Sinais sem Ruído)

\centering

![Erro quadrático médio das reconstruções para cada algoritmo](./figs_no_noise/RMSE_comp.png){ width=250px }

<!--## Resultados (Sinais sem Ruído)

\centering

![](./figs_no_noise/Lag_comp.png){ width=350px } -->

## Resultados (Sinais sem Ruído)

\centering

![Recuperação de k1 via algoritmo Passo Fixo](./figs_no_noise/k1_random_fix.png){ width=150px }

## Resultados (Sinais sem Ruído)

\centering

![Recuperação de k1 via algoritmo Seção Áurea](./figs_no_noise/k1_random_secau.png){ width=150px }

## Resultados (Sinais sem Ruído)

\centering

![Recuperação de k1 via algoritmo Barziliai-Browein](./figs_no_noise/output_k1_bb.png){ width=150px }

## Resultados (Sinais sem Ruído)

\centering

![Recuperação de k2 via algoritmo Passo Fixo](./figs_no_noise/k2_random_fix.png){ width=150px }

## Resultados (Sinais sem Ruído)

\centering

![Recuperação de k1 via algoritmo Seção Áurea](./figs_no_noise/output_k2_secau.png){ width=150px }

## Resultados (Sinais sem Ruído)

\centering

![Recuperação de k2 via algoritmo Busca Global](./figs_no_noise/k2_random_global.png){ width=150px }

## Resultados (Sinais sem Ruído)

\centering

![Recuperação de k2 via algoritmo Barziliai-Browein](./figs_no_noise/output_k2_bb.png){ width=150px }

## Resultados (Sinais Ruidosos)

\centering

![Erro quad. médio usando busca global com 30 dB de SNR (*kernel* simplificado)](./figs_SNR_30dB/RMS_simp_global_noise.png){ width=350px }

## Resultados (Sinais Ruidosos)

\centering

![Função de custo usando busca global com 30 dB de SNR (*kernel* simplificado)](./figs_SNR_30dB/cost_simp_global_noise.png){ width=350px }

## Resultados (Sinais Ruidosos)

\centering

![Recuperação do *kernel* simplificado 1](./figs/k1_simp_global_noise.png){ width=150px }

## Resultados (Sinais Ruidosos)

\centering

![Recuperação do *kernel* simplificado 2](./figs/k2_simp_global_noise.png){ width=150px }

## Resultados (Sinais Ruidosos)

\centering

![Recuperação da imagem (*kernel* simplificado)](./figs/recup_simp_global_noise.png){ width=150px }

## Resultados (Sinais Ruidosos)

\centering

![Erro quad. médio usando busca global com 30 dB de SNR (*kernel* aleatório)](./figs_SNR_30dB/RMS_random_global_noise.png){ width=350px }

## Resultados (Sinais Ruidosos)

\centering

![Função de custo usando busca global com 30 dB de SNR (*kernel* aleatório)](./figs_SNR_30dB/cost_random_global_noise.png){ width=350px }

## Resultados (Sinais Ruidosos)

\centering

![Recuperação do *kernel* aleatório 1](./figs/k1_random_global_noise.png){ width=150px }

## Resultados (Sinais Ruidosos)

\centering

![Recuperação do *kernel* aleatório 2](./figs/k2_random_global_noise.png){ width=150px }

## Resultados (Sinais Ruidosos)

\centering

![Recuperação da imagem (*kernel* aleatório)](./figs/recup_random_global_noise.png){ width=150px }

# Cronograma

## Cronograma Proposto

\begin{table}
	% \centering
	\begin{tabular}{|r|c|c|c|c|c|c|}
		\hline
		\backslashbox{Atividade}{Mês} & Jan & Fev & Março & Abril & Maio & Junho \\
		\hline
		Revisão da Literatura & X & X & X & X & X & X \\
		\hline
		Implementação & X & X & X & X & & \\
		\hline
		Testes com dados reais &  & X & X & X & X & \\
		\hline
		Redação da dissertação & X & X & X & X & X & X \\
		\hline
		Submissão de artigo & X & X & X & X & X & X \\
		\hline
	\end{tabular}
\end{table}

# Conclusões

## Conclusões

* Há um efeito determinante do método de busca de passo sobre a taxa de convergência do algoritmo

* Percebeu-se que algoritmos menos custosos computacionalmente podem ter desempenhos satisfatório

* Boas soluções para $x$ e soluções nem tão boas para $k_i$ 
	* Regularização em $k_i$?

## Conclusões

* Quando ruidoso o problema exige alguma técnica de regularização para o problema em si

* O algoritmo RED, quando bem ajustado, propõe soluções interessantes para a regularização 

\phantom{uuuuui}

### Perspectivas futuras

* Acertar valor ótimo das constantes $\lambda$ e $\mu$ para cenários de baixa SNR

* Verificar possíveis regularizações para $k_i$

##

\centering
\Large \textbf{Muito obrigado pela Atenção!}

## Referências

\centering

![](./figs/ref1.png){ width=350px }

## Referências

\centering

![](./figs/ref2.png){ width=350px }
