# Importacao de modulos usados no script
from netCDF4 import Dataset
from matplotlib import pyplot as plt
import numpy as np

# Inicio da manipulaçao dos dados satelitais usados
nome1 = 'OR_ABI-L2-CMIPF-M6C02_G16_s20230670000203_e20230670009511_c20230670009596.nc' #'OR_ABI-L2-CMIPF-M6C13_G16_s20230670000203_e20230670009525_c20230670009599.nc'
nome2 = 'OR_ABI-L2-CMIPF-M6C13_G16_s20230670010203_e20230670019527_c20230670019595.nc'
nome3 = 'OR_ABI-L2-CMIPF-M6C02_G16_s20230670050203_e20230670059511_c20230670059583.nc' #'OR_ABI-L2-CMIPF-M6C13_G16_s20230670050203_e20230670059523_c20230670059588.nc'

data1 = Dataset('satelite_img/' + nome1)
data2 = Dataset('satelite_img/' + nome2)
data3 = Dataset('satelite_img/' + nome3)

# Recuperaçao de infos de aquisiçao da imagem
start1 = nome1[(nome1.find('s') + 1) : nome1.find('_e')]
start2 = nome2[(nome2.find('s') + 1) : nome2.find('_e')]
start3 = nome3[(nome3.find('s') + 1) : nome3.find('_e')]

end1   = nome1[(nome1.find('e') + 1) : nome1.find('_c')]
end2   = nome2[(nome2.find('e') + 1) : nome2.find('_c')]
end3   = nome3[(nome3.find('e') + 1) : nome3.find('_c')]

# Formataçao de texto para aquisiçao
title1 = start1[0:4] + ' dia ' + start1[4:7] + ' - ' + start1[7:9] + ':' + start1[9:11] + ':' + start1[11:13] + '.' + start1[13:14] + ' utc'
title2 = start2[0:4] + ' dia ' + start2[4:7] + ' - ' + start2[7:9] + ':' + start2[9:11] + ':' + start2[11:13] + '.' + start2[13:14] + ' utc'
title3 = start3[0:4] + ' dia ' + start3[4:7] + ' - ' + start3[7:9] + ':' + start3[9:11] + ':' + start3[11:13] + '.' + start3[13:14] + ' utc'

# Recuperaçao de imagens e fragmentos de interesse
var1  = data1.variables['CMI'][::8, ::8]
var2  = data3.variables['CMI'][::8, ::8]
frag1 = var1[500:1500, 500:1500]
frag2 = var2[500:1500, 500:1500]

# Recuperaçao de informaçoes geograficas das imagens
geo_estent1 = data1.variables['geospatial_lat_lon_extent']
geo_estent2 = data2.variables['geospatial_lat_lon_extent']
geo_estent3 = data3.variables['geospatial_lat_lon_extent']

cen1 = str(geo_estent1.geospatial_lon_center)
wes1 = str(geo_estent1.geospatial_westbound_longitude)
eas1 = str(geo_estent1.geospatial_eastbound_longitude)
nor1 = str(geo_estent1.geospatial_northbound_latitude)
sou1 = str(geo_estent1.geospatial_southbound_latitude)

cen2 = str(geo_estent2.geospatial_lon_center)
wes2 = str(geo_estent2.geospatial_westbound_longitude)
eas2 = str(geo_estent2.geospatial_eastbound_longitude)
nor2 = str(geo_estent2.geospatial_northbound_latitude)
sou2 = str(geo_estent2.geospatial_southbound_latitude)

cen3 = str(geo_estent3.geospatial_lon_center)
wes3 = str(geo_estent3.geospatial_westbound_longitude)
eas3 = str(geo_estent3.geospatial_eastbound_longitude)
nor3 = str(geo_estent3.geospatial_northbound_latitude)
sou3 = str(geo_estent3.geospatial_southbound_latitude)

# Formataçao de strings com info geograficas da img
text1 = 'Extensao Geoespacial\n' + wes1 + '°W\n' + eas1 + '°E\n' + nor1 + '°N\n' + sou1 + '°S\n' + 'centro = ' + cen1 + '°'
text2 = 'Extensao Geoespacial\n' + wes2 + '°W\n' + eas2 + '°E\n' + nor2 + '°N\n' + sou2 + '°S\n' + 'centro = ' + cen2 + '°'
text3 = 'Extensao Geoespacial\n' + wes3 + '°W\n' + eas3 + '°E\n' + nor3 + '°N\n' + sou3 + '°S\n' + 'centro = ' + cen3 + '°'

# Exploraçao dos campos de variaveis no dispositivo
diff1 = np.abs(var1 - var2)
diff2 = np.abs(frag1 - frag2)
print(diff1.shape)
#print(f'max diff1: {np.max(diff1)}')
#print(f'max diff2: {np.max(diff2)}')
#print(f'max var1: {np.max(var1)}')
#print(f'max var2: {np.max(var2)}')

# Plotagem das imagens adquiridas
plt.figure(figsize=(16, 16))

# Globo 1
plt.subplot(2, 3, 1)
plt.axis('off')
plt.title('Globo ' + title1)
plt.text(-1, -1, text1, fontsize=7)
plt.imshow(var1)

# Globo 2
plt.subplot(2, 3, 2)
plt.axis('off')
plt.title('Globo ' + title3)
plt.text(-1, -1, text3, fontsize=7)
plt.imshow(var2)

# Difereça de globos
plt.subplot(2, 3, 3)
plt.axis('off')
plt.title('Diferença entre intervalos')
plt.imshow(diff1)

# Frag 1
plt.subplot(2, 3, 4)
plt.axis('off')
plt.title('Fragmento ' + title1)
plt.text(-1, -1, text1, fontsize=7)
plt.imshow(frag1)

# Frag 2
plt.subplot(2, 3, 5)
plt.axis('off')
plt.title('Fragmento ' + title3)
plt.text(-1, -1, text3, fontsize=7)
plt.imshow(frag2)

# Diferença de fragmentos
plt.subplot(2, 3, 6)
plt.axis('off')
plt.title('Diferença entre intervalos')
plt.imshow(diff2)
plt.show()
